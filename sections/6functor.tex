\section{Recollection: Quasi-coherent sheaves and cohomology}
\subsection{$\ox$-modules and (quasi-)coherent sheaves}

\begin{defn}
  Let $(X,\ox)$ be a locally ringed space.
  A \emph{sheaf of $\ox$-modules} on $X$ consists of
  \begin{itemize}
    \item
      an abelian sheaf $\shf$ on $X$; and 
    \item
      a morphism of sheaves $\ox \times \shf \to \shf$, such that for every $U\sse X$ open, the resulting morphism $\ox(U) \times \shf(U) \to \shf(U)$ turn $\shf(U)$ into an $\ox(U)$-module.
  \end{itemize}
  A morphism of $\ox$-modules $g\mc \shf \to \shg$ is a morphism of presheaves, such that for every $U\sse X$ open, the resulting map $\shf(U) \to \shg(U)$ is $\ox(U)$-linear.
  The resulting category is denoted by $\modcat(\ox)$.
\end{defn}

\begin{defn}
  \leavevmode
  \begin{enumerate}
    \item
      Let $X = \spec(A)$ be an affine scheme and $M$ an $A$-module.
      The $\ox$-module $\wtilde{M}$ is defined via $\wtilde{M}(D(f)) \defined M[f^{-1}]$ for $f\in A$.
      This gives a fully faithful functor
      \[
        \qcoh{-}
        \mc
        \modcat(A)
        \to 
        \modcat(\ox),~
        M \mapsto \wtilde{M}.
      \]
    \item 
      If $X$ is any scheme (i.e. not necessarily affine), then an $\ox$-module $\shf$ is called \emph{quasi-coherent} if there is an open affine cover $X = \bunion U_i$ with $U_i = \spec(A_i)$ and $A_i$-modules $M_i$ such that $\restrict{\shf}{U_i} \cong \wtilde{M_i}$ holds.
      We denote the full subcategory of $\modcat(\ox)$ that is spanned by the quasi-coherent modules as $\qcohcat(X)$.
    \item
      Assume now that $X$ is noetherian (but still not necessarily affine).
      Then a quasi-coherent module $\shf$ on $X$ is called \emph{coherent} if in the above cover, each of the $M_i$ is of finite type over the respective $A_i$.
      We denote the full subcategory of coherent modules by $\cohcat(X)$. 
  \end{enumerate}
\end{defn}
Note that tautologically (with this definition), the functor $\qcoh{-}$ induces an equivalence of categories $\modcat(A) \isomorphism \qcohcat(\spec(A))$.

\begin{rem}
  \leavevmode
  \begin{enumerate}
    \item
      Let $X$ be a scheme and $\shf$ an $\ox$-module.
      Then $\shf$ is quasi-coherent if and only if for every open affine $U = \spec(A) \sse X$ there is an $A$-module $M$ such that $\restrict{\shf}{U} \cong \wtilde{M}$ holds.
    \item 
      Assume that $X = \spec(A)$ and that $A$ is noetherian. 
      Then a quasi-coherent $\ox$-module $\shf$ is coherent if and only if $\hh^0(X,\shf)$ is a finite-projective $A$-module.
  \end{enumerate}
\end{rem}

\begin{prop}
  Let $X$ be a scheme.
  The categories $\modcat(X)$, $\qcohcat(X)$ and $\cohcat(X)$ are abelian.
\end{prop}

\begin{defn}
  Let $(X,\ox)$ be a locally ringed space, and let $\shf,\shg \in \modcat(\ox)$.
  Their \emph{tensor product} $\shf\tensor_{\ox} \shg$ is defined as the sheafification of the assignement 
  \[
    U \mapsto 
    \shf(U)
    \tensor_{\ox(U)}
    \shg(U).
    \]
\end{defn}
If $X = \spec(A)$ is an affine scheme and $\shf = \wtilde{M}$, $\shg = \wtilde{N}$ are both quasi-coherent, then there is a canonical isomorphism 
\[
  \shf \tensor_{\ox} \shg 
  \cong 
  \qcoh{M \tensor_A N}.
\]

\begin{defn}
  Again, let $(X,\ox)$ be a ringed space and let $\shf,\shg\in \modcat(\ox)$.
  Their \emph{(internal) Hom-sheaf} $\shom_{\ox}(\shf,\shg)$ is defined as (the sheafification of) the assignement 
  \[
    U \mapsto \hom_{\oo_{U}}(\restrict{\shf}{U},\restrict{\shg}{U}).
  \]
\end{defn}

\begin{rem}
  If $X$ is a scheme and both $\shf$ and $\shg$ are quasi-coherent, then there are natural isomorphisms
  \[
    \shom(\shf,\shg)(U)
    \cong 
    \hom_{\ox(U)}(\shf(U),\shg(U))
  \]
  But in general, $\shom(\shf,\shg)$ is itself not quasi-coherent.
  A necessary condition for this is that $\shf$ is coherent.
\end{rem}

\begin{prop}
  Let $(X,\ox)$ be a ringed space.
  Then the functors $\tensor_{\ox}$ and $\shom_{\ox}$ are adjoint, in the sense that for all $\ox$-modules $\shf,\shg,\shh$, there is a natural isomorphism 
  \begin{align*}
    \shom_{\ox}(\shf \tensor_{\ox} \shg, \shh)
    &\cong
    \shom_{\ox}(\shf, \shom_{\ox}(\shg,\shh)).
    \intertext{In particular, there are isomorphisms of abelian groups}
    \hom_{\ox}(\shf \tensor_{\ox} \shg, \shh)
    &\cong
    \hom_{\ox}(\shf, \shom_{\ox}(\shg,\shh)).
  \end{align*}
\end{prop}

\begin{construction}
  Let $f\mc X \to Y$ be a morphism of schemes, $\shf \in \modcat(\ox)$ and $\shg \in \modcat(\oo_Y)$.
  \begin{enumerate}
    \item
      The \emph{pushforward} $f_{\ast}\shf$ is defined as usual, and readily a module over $\oo_Y$.
    \item
      The sheaf-theoretic pullback $f^{-1}\shg$ is only a module over $f^{-1}\oo_Y$.
      The latter is however a module over $\ox$, so we define the \emph{pullback} of the $\oo_Y$-module $\shg$ as 
      \[
        f^{\ast}\shg 
        \defined 
        f^{-1}\oo_Y \tensor_{\ox} f^{-1}\shg.
      \]
  \end{enumerate}
  These two are adjoint, i.e. form an adjunction 
  \[
    f^{\ast}
    \mc 
    \modcat(\oo_Y)
    \leftrightarrows
    \modcat(\ox)
    \mcc
    f_{\ast}
    \]
    with $f^{\ast}$ the left-adjoint and $f_{\ast}$ the right-adjoint.
\end{construction}

\begin{prop}
  Let $f\mc C \to Y$ be a morphism of schemes.
  \begin{enumerate}
    \item 
      \label{qcoh-under-pullback}
      If $\shg \in \qcohcat(Y)$ is quasi-coherent, then so is $f^{\ast}\shg$.
    \item
      \label{qcoh-under-pushforward}
      If $\shf \in \qcohcat(X)$ is quasi-coherent and moreover $f$ qcqs, then $f_{\ast}\shf$ is quasi-coherent too.
  \end{enumerate}
\end{prop}

\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item
      The key is the observation that if $X$ and $Y$ are both affine, say $X = \spec(B)$ and $Y= \spec(A)$, and $\shg = \wtilde{M}$, then
      \begin{equation}
        f^{\ast}\shg = \qcoh{M\tensor_A B}
      \end{equation}
      holds.
      As the claim is local on $X$ and $Y$, this actually suffices.
    \item 
      ...
  \end{enumerate}
\end{proof}

\begin{rem}
  Part \ref{qcoh-under-pullback} holds also with ``coherent'' in place of ``quasi-coherent'' (because being finite-projective is stable under tensor products); this is however generally not (emphasis added) true for \ref{qcoh-under-pushforward}.
  The failure of pushforwards of coherent sheaves to be coherent again can already be seen in the affine setting: if $k$ is a field and $\affa_k^1 = \spec(k[t])$ the affine line with structure morphism $f\mc \affa_k^1 \to \spec(k)$, then $f_{\ast}\oo_{\spec(k[t])}  k[t]$ which is no longer a finitely generated $k$-module.
  We will recover the preservation of coherence under pushforward in a different setting though, namely if $f$ is projective (c.f. \cref{proper-pushforward}).
\end{rem}

 \subsection{Derived functors}

In the lecture, we recollected some facts about the derived category of an abelian category.
I shall not attempt to reproduce them here --- the reader is refered to \cite{huybrechts} (next to the ``standard'' literature) for a summary.
Throughout, we will make use of the fact that $\modcat(\ox)$ has enough injectives, and so in particular, we get an identification 
\[
  K^+(\injobj(\modcat(\ox))) \isomorphism D^+(\modcat(\ox)),
\]
i.e. objects in the bounded-below derived category can be represented by injective resolutions and right-derived functors can be computed on injective resolutions.

\begin{defn}
  Both the functors 
  \[
    \hom_{\ox}(\shf,-)
    \mc 
    \modcat(\ox)
    \to 
    \abcat
    \text{ and }
    \shom(\shf,-)\mc
    \modcat(\ox)
    \to 
    \modcat(\ox)
  \]
  are left-exact.
  Their $i$-th right derived functors are respecitivly denoted as $\ext^i(\shf,-)$ and $\shext^i(\shf,-)$.
\end{defn}

\begin{rem}
  \leavevmode
  \begin{enumerate}
    \item
      If $U \sse X$ is an open subset and $G$ an injective $\ox$-module, then the restriction $\restrict{\shg}{U}$ is again an injective $\oo_U$-module.
      So in particular, we have 
      \[
        \shext^i_{\ox}(\shf,\shf')
        \cong 
        \shext^i_{\oo_U}(\restrict{\shf}{U}, \restrict{\shf'}{U})
      \]
      for all $\shf,\shf'\in \modcat{\ox}$.
    If $\shf$ is coherent, and $\shg$ coherent/quasi-coherent, then $\ext^i{\shf,\shg}$ is again coherent/quasi-coherent.
    \item
      If $\shf,\shg$ are $\ox$-modules, then there are natural isomorphism 
        \[
          \hom_{D(\modcat(\ox))}(\shf,\shg[i])
          \cong 
          \ext^i(\shf,\shg),
        \]
        so composition induces a pairing 
        \[
          \ext^i(\shf,\shg)
          \times 
          \ext^j(\shg,\shh)
          \to 
          \ext^{i+j}(\shf,\shh),
        \]
        which is called the \emph{Yoneda pairing}.
    \end{enumerate}
\end{rem}

\begin{defn}
  Let $f\mc X\to Y$ be a morphism of schemes.
  Then $f_{\ast}$ is left-exact, so we get the right derived functor 
  \[
    Rf_{\ast}\mc 
    D^+(\modcat(\ox))
    \to 
    D^+(\modcat(\ox)).
  \]
  The $i$-th right derived functor is sometimes called the \emph{$i$-th higher direct image}.
\end{defn}

\begin{prop}
  Let $f\mc X \to Y$ be a qcqs-morphism and $\shf\in \qcohcat(X)$.
  Then it holds that $R^if_{\ast}\shf \in \qcohcat(Y)$ for all $i \geq 0$.
  For an open subset $V \sse Y$, it holds that 
  \[
    \left(R^if_{\ast}\shf\right)(V)
    =
    \hh^i(f^{-1}(V),\restrict{\shf}{f^{-1}(V)}).
  \]
\end{prop}

\begin{theorem}
  \label{proper-pushforward}
  Let $f\mc X\to Y$ be a proper morphism of coherent sheaves, and $\shf \in \cohcat(X)$ a coherent module over $X$.
  Then for all $i \geq 0$, it holds that $R^if_{\ast}\shf$ is a coherent module over $Y$.
\end{theorem}

\subsection{Serre duality}
\begin{theorem}
  Let $k$ be a field and $X$ a $n$-dimensional smooth projective scheme over $k$.
  Set $\omega_X \defined \Omega^n_{X/k}$.
  Then there is a function 
  \[
    \mathrm{Tr}\mc 
    \hh^n(X,\omega_X)
    \to 
    k,
  \]
  such that for all $\shf \in \qcohcat(X)$, the induced map 
  \[
    \hh^i(X,\shf)
    \tensor_k
    \ext^{n-i}(\shf,\omega_X)
    \to 
    k
  \]
  is a perfect pairing.
  This map is obtained from the composite 
  \[
    \begin{tikzcd}
      \ext^i_{\ox}(\ox,\shf) 
      \tensor_k
      \ext^{n-i}(\shf,\omega_X)
      \ar{d}[right]{\text{Yoneda product}}
      \\
      \ext^n(\ox,\omega_x)
      \ar{d}
      \\
      k
    \end{tikzcd}
  \]
  after the identification $\ext^j(\ox,\shf) = \hh^j(X,\shf)$ for any $\ox$-module $\shf$.
\end{theorem}


