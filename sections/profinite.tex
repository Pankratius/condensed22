\section{Basics of condensed sets}

\subsection{Sites and sheaves}

\begin{defn}
  A \emph{site} is a category $\ccat$ and a set $\covers(\ccat)$ of families of morphisms $\lset f_i \mc U_i \to U\rset_{I}$ called \emph{coverings}, satisfying the following conditions:
  \begin{enumerate}
    \item
      If $V\isomorphism U$ is an isomorphism in $\ccat$, then $\lset V \isomorphism U \rset$ is a covering;
    \item
      For $\lset f_i \mc U_i \to U\rset$ and $\lset f_{ij}\mc U_{ij} \to U_i \rset_{j \in I_i}$ coverings, it holds that $\{ U_{ij} \xrightarrow{f_{ij}} U_i \xrightarrow{f_i} U \}_{j\in I_j,i\in I}$ is again a covering;
    \item
      If $\lset f_i \mc U_i \to U\rset_{i\in I}$ is a covering and $V\to U$ any morphism in $\ccat$, then the fiber product $U_i \times V$ exists for all $i \in I$, and the collection of morphisms $\lset U_i\times V \to V\rset_{i\in I}$ in the diagrams 
      \[
        \begin{tikzcd}
          V \times U_i 
          \ar{d}
          \ar{r}
          &
          U_i
          \ar{d}[right]{f_i}
          \\
          V
          \ar{r}
          &
          U
        \end{tikzcd}
      \]
      is again a covering.
  \end{enumerate}
\end{defn}

\begin{example}
  If $X$ is a topological space, then we can consider the category $\ouvcat(X)$ of open subsets.
  This becomes a site by declaring a family of open subsets $U_i \sse U$ to be a covering (of $U$) if the $U_i$ well ... cover $U$ in the usual sense (i.e. $\bunion U_i = U$).
  Slightly more abstract, we can also define this by setting $\covers(\ouvcat(X))$ to consist of all the collections  
  \[
    \lset (f_i \mc U_i \hookrightarrow U)_i \ssp \begin{array}{l} \text{$f_i$ are open immersions} \\ \text{$\sqcup f_i \mc \bigsqcup U_i \to U$ is surjective}\end{array}
    \rset
  \]
  for the collection $U \hookrightarrow X$ of open immersions ``over'' $X$.
  Oftentimes, the sites we are encountering in algebraic geometry follow a similar pattern --- objects of the category are given by morphisms to a specified base ``space'' with a ``well-behaved'' property, and then coverings are given again by such morphisms that are required to be jointly surjective.\footnote{In one other approach, one drops the assumption on the morphism of the objects over $X$ (and keeps it only for the coverings). This is then called the ``big ... site''.}
  \par 
  For example, the \emph{(small) étale site} of a scheme $X$ has as underlying category the full subcategory of $\schemecat_{/X}$ of schemes that are étale over $X$; coverings of $U \xrightarrow{\text{étale}} X$ are given by maps $f_i\mc U_i \xrightarrow{\text{étale}} U$ such that $\amalg f_i$ surjects onto $U$.
  (For this to be a site, we need that étale maps are stable under base change and composition, both of which is true).
  This site is denoted as $X\ets$.
  By replacing ``étale'' with ``fppf'', ``smooth'' or ``open immersion'', we obtain the fppf, smooth and Zariski site of $X$, respectivly.
  Note that the Zariski site $X\zars$ of a scheme $X$ is tautologically the same as the site we defined above for general topological spaces. 
\end{example}

\begin{defn}
  Let $\ccat$ be a site, $\dcat$ any cocomplete category and $\shf\mc \ccat\op \to \dcat$ a functor.
  We say that $\shf$ is a \emph{$\dcat$-valued sheaf on $\ccat$} if for all covers $\lset f_i \mc U_i \to U\rset \in \covers(\ccat)$, the diagram 
  \[
    \begin{tikzcd}
      \shf(U)
      \ar{r}
      &
      \displaystyle
      \prod_i \shf(U_i)
      \ar[shift left = 1]{r}[above]{\projection_1}
      \ar[shift right = 1]{r}[below]{\projection_2}
      &
      \displaystyle
      \prod_{i,j} \shf(U_i \times_U U_j)
    \end{tikzcd}
  \]
  is an equalizer diagram in $\dcat$.
  Here, the maps $\projection_1$ and $\projection_2$ are induced by 
  \[
    U_i \times_U U_j
    \twoheadrightarrow
    U_i
    \xrightarrow{f_i}
    U
    \text{ respectively }
    U_i \times_U U_j 
    \twoheadrightarrow
    U_j
    \xrightarrow{f_j}
    U.
  \]
  We denote the full subcategory of $\presheafcat_{\dcat}(\ccat)$ that is spanned by the $\dcat$-valued sheaves as $\sheafcat_{\dcat}(\ccat)$.
  If $\dcat = \abcat$ (or if it admits a reasonable forgetful functor to $\abcat$), then we usually drop the $\dcat$ from the notation and only write $\sheafcat(\ccat)$.
\end{defn}
 
\begin{example}
  If $\ccat = \ouvcat(X)$ is the site associated to a topological space $X$, then this notion of sheaves coincides with the ``usual'' notion of sheaves on a topological space.
  In particular, if $X$ is a scheme, then sheaves $X\zars$ are the sheaves we are used to from algebraic geometry.
  For the étale site, sheaves on $X\ets$ are the ones used in the famous theory of étale cohomology (as we have a general notion for cohomology of sheaves on sites).
\end{example}

\subsection{Cofiltered limits}

\begin{defn}
  Let $\ccat$ be a category and $M\mc I \to \ccat$ a diagram in $\ccat$.
  We say that this diagram is \emph{cofiltered} if it satisfies the following two conditions:
  \begin{enumerate}
    \item
      For all $i,j\in I$, there is a $k\in I$ and morphisms $k\to I$, $j \to I$;
    \item
      For all $i,j\in I$ and all morphisms $a,b\mc i \to j$, there exists a morphism $c\mc k \to i$ such that $M(a\circ c) = M(b\circ c)$ holds in $\ccat$.
  \end{enumerate}
  We say that the category $I$ is cofiltered if $\id\mc I \to I$ is a cofiltered diagram in the above sense.
\end{defn}

\begin{rem}
  Cofiltered diagrams should be the same as ``direct inverse (aka projective) diagrams''.
  While not every inverse diagram is directed (aka not every preordered set is totally ordered), limits over arbitrary inverse systems can be computed as limits over cofiltered diagrams (c.f. \cite{stacks}*{\stackstag{0032}}).
  In particular, we will sometimes use the symbol $\varprojlim$ to indicate that we are taking the limit over a cofiltered/inverse system.
\end{rem}

\subsection{Profinite spaces}
\begin{defn}
  Let $X$ be a topological space.
  \begin{enumerate}
    \item 
      We say that $X$ is \emph{totally disconnected} if it has no non-trivial connected subsets.
    \item 
      We say that $X$ is \emph{totally seperated} if for all $x,y\in X$, there exist disjoint open subsets $U,V$ containing $x,y$ respectively, such that $X = U \sqcup V$ holds.
    \item
      We say that $X$ is \emph{profinite} if it it is homeomorphic to a limit of a diagram of finite and discrete topological spaces.
  \end{enumerate}
\end{defn}
As a family of examples, we have that finite discrete spaces have all of the three above properties. 


\begin{prop}
  Let $X$ be a topological space.
  The following are equivalent:
  \begin{enumerate}
    \item 
      \label{equivalent-profinite:def}
      $X$ is profinite.
    \item
      \label{equivalent-profinite:good}
      $X$ is seperated, (quasi-)compact and totally disconnected.
  \end{enumerate}
  In this case, $X$ is actually homeomorphic to a cofiltered limit of discrete, finite sets.
\end{prop}

\begin{proof}
  Assume \ref{equivalent-profinite:def}, and choose a diagram $(X_i)_{i\in I}$ of finite discrete spaces $X_i$, such that $X \cong \lim X_i$ (note that we do not(!) assume that $I$ is directed):
  \begin{itemize}
    \item
    As each of the $X_i$ is (quasi)-compact and being quasi-compact is preserved under arbitrary limits, we find that $X$ is itself quasi-compact:
    To see that being quasi-compacts is stable under arbitrary limits, we first note that $\lim X_i \sse \prod X_i$ holds. 
    Now since each of the $X_i$ is compact, Tychnoff's theorem tells us that their product is too. 
    Hence it suffices to show that $\lim X_i$ is closed in the product, which holds because the graph of every transition map is closed (because the spaces in question are Hausdorff).
  \item 
    To show that $X$ is seperated, let $x\neq y \in X$ be two distinct points --- we have to show that there are open neighborhoods $x\in U_x$ and $y\in U_y$ such that $U_x \cap U_y = \emptyset$.
    By the limit description for $X$, we can find a $i$ such that $x,y$ map to distinct points in $X_i$.
    As $X_i$ is itself seperated, we can pull-back any two open neighborhoods of $x,y$ in $X_i$ to $X$ to get the desired neighborhoods in $X$.
  \item
    Finally, to get that $X$ is totally disconnected, we use that $X$ is locally compact (because it is quasi-compact and seperated, the notions of being ``totally disconnected'' and ``totally seperated'' coincide (c.f. \cref{app:tdisc-iff-tsep})).
    The argument that a limit of finite discrete (so in particular totally seperated) spaces is again totally seperated is the same as for totally disconnected.
  \end{itemize}
  Conversely, assume \ref{equivalent-profinite:good}.
  We now need to produce an index set $I$ to take the limit over.
  For that, we define $I$ to be the set of all finite disjoint union decompositions of $X$:
  \[
    I
    \defined 
    \lset 
    \lset U_i\rset_{\text{finite}}
    \ssp 
    X = \bigsqcup_{\text{finite}} U_i
    \text{, $U_i \sse X$ open}
    \rset.
  \]
  Then $I$ is partially ordered via refinement (i.e. for $i,j\in I$, we set $i\leq j$ if and only if the covering $j$ refines the covering $i$).
  Now by construction, each of the $X_i$ is a finite set, and we also have for every covering $i\in I$ a canonical map $X \to i$ that sends a point $x\in X$ to its component under the decomposition of $X$ corresponding to $i$.
  This induces a continous map 
  \[
    X \to \lim_{i\in I} i,
  \]
  which we claim to be a homeomorphism.
  First of all, since $X$ is assumed to be quasi-compact and $\lim_{i\in I} i$ is Hausdorff (as a limit of Hausdorff spaces, like we've argued above), it suffices to show that $f$ is bijective.

\end{proof}


\begin{defn}
  \leavevmode
  \begin{enumerate}
    \item
      \label{proetale-site-defn}
      The \emph{proétale site of a point}, $\ast\pets$, is the category of profinite sets with continous morphisms\footnote{I think people use ``profinite set'' and ``profinite topological space'' somewhat interchangeably. But nonetheless, a ``morphism of profinite sets'' should always be continous for the (canonical) topology the sets carry.}, and coverings given by finite families of jointly surjective maps.\glsadd{petal-of-point}
    \item
      A \emph{condensed set} is a sheaf of sets on $\ast\pets$.
      In other words, a condensed set is a functor 
      \[
        T\mc
        \lset \text{profinite sets} \rset \op
        \to 
        \setcat,
        \]
        satisfying $T(\emptyset) = \ast$ and the following two other sheaf conditions:
        \begin{itemize}
          \item 
            For all profinite sets $S_1,S_2$ the natural map 
            \[
              T(S_1\sqcup S_2)
              \to 
              T(S_1) \times T(S_2)
            \]
            is a bijection;
          \item 
          For any surjection of profinite sets $f\mc S'\twoheadrightarrow S$ with fiber product $S' \times_S S' \to S$ and the two projections $\projection_1,\projection_2$, the assignement $x \mapsto T(f)(x)$ induces a bijection 
          \[
            T(S)
            \isomorphism
            \lset 
            y\in T(S')
            \ssp 
            T(\projection_1)(y)
            =
            T(\projection_2)(y)
            \in 
            T(S' \times_S S')
            \rset.
          \]
        \end{itemize}
        We denote the sheaf category of condensed sets by $\condset$. \glsadd{cond-set}
      \item 
        In a similar vein, we define a \emph{condesed abelian group/condensed ring} to be a sheaf of abelian groups/rings on $\ast\pets$.
        These categories are denoted by $\condab$ and $\cond(\ringcat)$, respectively. 
  \end{enumerate}
\end{defn}

\begin{rem}
  Probably some explanations are in order as to why the site in \ref{proetale-site-defn} caries this particular name.
  A good starting point is maybe the following --- for a field $k$, classical étale theory tells us that there is a bijection between étale $k$-schemes and finite discrete sets with a $G$-action, where $G \defined \gal(k^\mathrm{sep}/k)$ is the absolute Galois group of $k$:
  \begin{align*}
    \lset
    \text{finite étale $k$-schemes}
    \rset 
    \isomorphism
    &
    \lset
    \text{finite discrete sets with $G$-action}
    \rset
    \\
    X
    \longmapsto
    &
    \lset 
    x\mc k^\mathrm{sep}
    \to 
    X
    \rset
  \end{align*}
  In particular, if $k=k^\mathrm{sep}$ (so a ``geometric point''), then $\ast\ets = \mathrm{FinSet}$.
  \par 
  In \cite{proetale}, the authors define the notion of a ``weakly-étale morphism'' for a general scheme.
  Since in the end we are in this course only interested in the case of an affine base scheme, we content ourselves with stating that a ring map $f\mc A\to B$ is etale if and only if it is weakly étale and finitely presented.
  The \emph{proétale site} of a scheme $X$ is then defined as the site of weakly-étale $X$-schemes, with covers given by fpqc-covers.
  The authors then show that for an affine scheme $\spec(B)$, the sites defined by weakly-étale algebras and by ind-étale algebras over the base ring $B$ (i.e. by $B$-algebras that can be written as filtered colimit of étale $B$-algebras) coincide \cite{proetale}*{Theorem 1.3}.
 tbc. 
\end{rem}

\begin{construction}
  \label{top-to-condensed}
  Let $X$ be a topological space.
  We associate to it a presheaf $\underline{X}$ on $\ast\pets$, via 
  \[
    S \mapsto \contmap(S,X).
  \]
  This actually defines a sheaf on $\ast\pets$:
  That the first condition (i.e. sending finite coproducts to products) holds is clear (without any assumption on $S$).
  For the second condition, we first note that we have this bijection if we drop the condition for the maps to be continous (because pushout squares in $\setcat$ along surjections are automatically bicartesian (emph!)).
  To see that it is also bijection for continous maps, we only have to aditionally verify that the preimage of a continous map under the above isomorphism is again continous --- this holds, since any surjective map $S\to S'$ of compact Hausdorff spaces is a quotient map (\cref{app:surjective-chaus-quotient}) or in other words, continuity can be checked after precomposing with $f$.\glsadd{assoc-cond}
  \par 
  Note that if $A$ is an abelian group, then $\underline{A}$ is a condensed abelian group.
\end{construction}
  Our next goal is to show that the functor $\underline{(-)}$ is fully faithful when restricted to a suitable subcategory of $\topcat$.
  For that, it will be advantageos to replace our ``model'' for condensed sets by two different ones (so the sites in question might be non-isomorphic, but the categories of ``sheaves'' on them will stay equivalent. Note that this is a familiar phaenomenon: for example, the small and the big étale site of a scheme are non-isomorphic, but lead to the equivalent categories of sheaves).

\begin{prop}
  The forgetful functor from the category $\topcat_{\mathrm{CHaus}}$ of compact Hausdorff spaces into the category of all topological spaces admits a left adjoint,
  \[
    \beta\mc \topcat \to \topcat_{\mathrm{CHaus}}.
  \]
  For a topological space $X$, the compact Hausdorff space $\beta X$ is called its \emph{Stone-\v{C}ech compactification}.\glsadd{sc-comp}
\end{prop}

\begin{proof}
  In the lecture, we mentioned that in ``favorable'' cases (we are oblivious to set-theory/the following product is well-defined), the space $\beta X$ can be constructed via the space
  \[
    Y \defined \prod_{\substack{f\mc X \to K\\ \text{$K$ compact}}}
    K,
  \]
  and then taking the closure of $X$ in it: in that case, we have that $Y$ is Hausdorff and compact (as a product of compact spaces, by Tychnoff), and hence $\beta X$ is compact-Hausdorff too (as it is, by construction, a closed subspace of a compact-Hausdorff space).
  \par 
  In general, one probably has to use ultrafilter (but we won't need the explicit construction, only the existence of the adjoint).
\end{proof}

\begin{defprop}
  Let $X$ be a compact-Hausdorff space. 
  Then the following are equivalent:
  \begin{enumerate}
    \item 
      The closure of every open subset of $X$ is open.
    \item
      $X$ is a projective object in the category $\topcat_{\mathrm{CHaus}}$ of compact-Hausdorff spaces: for every continous surjection $f\mc Y \to X$ with $Y$ compact-Hausdorff, there exists a continous $s\mc X \to Y$ such that $s\circ f = \id_{Y}$.
    \end{enumerate}
    In that case, we say that $X$ is \emph{extremally disconnected}.
\end{defprop}

\begin{prop}
  \label{extdisc-resol}
  Let $S$ be a compact-Hausdorff space.
  Then there exists a surjection $S'\twoheadrightarrow S$, with $S'$ an extremally disconnected space.
\end{prop}

\begin{proof}
  We claim that for $S_0 \defined S_{\mathrm{discrete}}$ (i.e. $S$, but with discrete topology), the space $S'\defined \beta S_0$ is extremally disconnected and admits a continous surjection $\beta S_0 \twoheadrightarrow S$: 
  Since $S_0$ is discrete, the canonical map $S_0 \to S'$ is a homeomorphism onto its image (c.f. \cref{app:sc-homeo-onto-image}).
  To see that $S$ is totally disconnected, we note that for any surjection of compact-Hausdorff spaces $Y\twoheadrightarrow Z$, the lifting problem 
  \[
    \begin{tikzcd}
      &
      &
      Y
      \ar[twoheadrightarrow]{d}
      \\
      S_0
      \ar[hookrightarrow]{r}
      \ar[dashed]{urr}[above left]{\exists h}
      &
      \beta S_0
      \ar{r}
      &
      Z
    \end{tikzcd}
  \]
  admits a solution (because $S_0$ is discrete).
  By the universal property of $\beta S_0$, we can extend this to a lift $\beta h$, such that the outermost triangle commutes.
  This alone doesn't imply that we have found a lift on all of $\beta S_0$!
  What saves us here is that $S_0$ is dense in $\beta S_0$, and since $Z$ is Hausdorff, equality of maps $\beta S_0 \to Z$ can be checked on any dense subset of $\beta S_0$.
  \par 
  To get the surjection $S'\twoheadrightarrow$, we note that the canonical map $S_0 \to S$ is continous, and since $S$ is compact-Hausdorff, it factors over $\beta S_0$, yielding the desired surjection.
\end{proof}

\begin{rem}
  One can ask the question how ``canonical'' the assignement $S\mapsto S'$ of the above proposition is.
  A partial answer is that $S'$ can always be chosen such that no proper closed subset of it maps surjectively onto $S$, and if this requiered, $S'$ is unique up to isomorphism (\cite{stacks}*{\stackstag{090D}}).
  As a (trivial) remark, we should also note that there is no good relation between $S$ and the space $S'$ we constructed if the space $S$ is extremally disconnected to begin with, since $S'$ only depends on the underlying set of $S$ (they are in particular not isomorphic).
  So while the assignement $S\mapsto S'$ of the proof above is functorial, we can \emph{not} use it to construct (fiber) products in $\topcat_{\mathrm{ext\text{-}disc}}$ (which cannot be given by the product in $\topcat$).
  This is different to somewhat similar situations where this (i.e. using a functor to transfer a product from an ``ambient'' category) works, for example compactly-generated Hausdorff space.
  Here, we actually even have an adjunction 
  \[
    (-)_c
    \mc
    \topcat_{\mathrm{comp\text{-}gen}}
    \leftrightarrows
    \topcat,
  \]
  but the point is that $(K)_c = K$ holds for any compactly-generated space $K$, which (to reiterate) is not the case for our functor from the proof and extremally disconnected topological spaces.
\end{rem}

\subsection{Different models for $\condset$}
So far, we have defined condensed sets as sheaves on the site $\ast\pets$.
We will now introduce two different ``sites'', and show that the categories of ``sheaves'' on them agree with our model for $\condset$.
Actually, one of these two ``sites'' actually \emph{isn't} a site, since it lacks fiber products (hence the quotation marks).
This seems to be a problem when it comes to spelling out a sheaf-condition.
Luckily, we can use the resolution procedure of \cref{extdisc-resol} to circumvent this problem.

\begin{defn}
  \leavevmode
  \begin{enumerate}
    \item
      The site $\ast^{\mathrm{CHaus}}\pets$ has objects given by compact Hausdorff spaces, and the same notion of covers as $\ast\pets$ (that is, finite families of jointly surjective maps).
    \item 
      The site $\ast^{\mathrm{ED}}\pets$ has objects given by extremally disconnected Hausdorff spaces, and the same notion of covers as $\ast\pets$.
  \end{enumerate}
\end{defn}

\begin{numtext}
  As already hinted at, $\ast^{\mathrm{ED}}\pets$ is not a site, since the category of extremally disconnected Hausdorff spaces does not have fiber products.
  We make the following definition:
  \begin{indefn}
    Let $T\mc \lset \text{extremally disconnected sets}\rset \op \to \setcat$ be a presheaf.
    We say that $T$ is a \emph{sheaf on} $\ast^{\mathrm{ED}}\pets$ if the natural map
    \[
      T(S_1 \sqcup S_2)
      \to 
      T(S_1)
      \times 
      T(S_2)
    \]
    is an isomorphism for all extremally disconnected sets $S_1,S_2$.
    We denote the full subcategory spanned by the sheaves on $\ast^{\mathrm{ED}}\pets$ by $\sheafcat(\ast^{\mathrm{ED}}\pets)$.
  \end{indefn}
  In that case, the reasonable analogue of the second part of the sheaf condition is also satisfied:
  \begin{inlem}
    Let $X$ be a sheaf on $\ast^{\mathrm{ED}}\pets$ (in the above sense).
    Let $f\mc S_1 \twoheadrightarrow S$ be a surjection of extremally disconnected sets.
    Consider a cartesian diagram in $\topcat$ of the form 
    \[
      \begin{tikzcd}
        S'
        \ar{r}[above]{\projection_1}
        \ar{d}[left]{\projection_2}
        \ar[phantom]{rd}{\lrcorner}
        &
        S_1
        \ar{d}[right]{f}
        \\
        S_1
        \ar{r}[below]{f}
        &
        S
      \end{tikzcd}
      \]
      Let $g\mc \gamma S'\twoheadrightarrow S'$ be a surjection onto $S'$ with $\gamma S'$ extremally disconnected.
      Then the natural map
      \[
        X(S)
        \longrightarrow
        \equalizer
        \left(
          \begin{tikzcd}[cramped]
          X(S_1) 
          \ar[shift left = 1]{r}[above]{(g\circ \projection_1)^\ast}
          \ar[shift right = 1]{r}[below]{(g\circ \projection_2)^\ast}
          &
          X(\gamma S')
        \end{tikzcd}
        \right)
      \]
      is an isomorphism.
  \end{inlem}
\end{numtext}

\begin{prop}
  \leavevmode
  \begin{enumerate}
    \item 
      Restriction along the inclusion $\scatfont{ProFin} \hookrightarrow \scatfont{CHaus}$ induces an equivalence of categories 
      \[
        \sheafcat(\ast^{\scatfont{CHaus}}\pets)
        \isomorphism 
        \sheafcat(\ast\pets).
      \]
    \item 
      Restriction along the inclusion $\scatfont{ED} \hookrightarrow \mathrm{ProFin}$ induces an equivalence of categories 
      \[
        \sheafcat(\ast\pets)
        \isomorphism 
        \sheafcat(\ast^{\mathrm{ED}}\pets).
      \]
  \end{enumerate}
\end{prop}
To summarize this proposition, we have the following (strict) inclusion of sites, that all nonetheless yield the same ``model'' of condensed sets:
\[
  \begin{tikzcd}[column sep = large]
    \mathrm{ED}
    \ar[hookrightarrow, r, shorten = 1cm]
    &
    \mathrm{ProFin}
    \ar[hookrightarrow, r, shorten = 1cm]
    &
    \mathrm{CHaus}
    \\
    \begin{array}{c}
    \text{\small intuitive spaces}
    \\
    \text{\small hard sheaf condition}
    \end{array}
    &
    \begin{array}{c}
    \text{\small less intuitive spaces}
    \\
    \text{\small easier sheaf condition} 
    \end{array}
    &
    \begin{array}{c}
    \text{\small weird spaces} 
    \\
    \text{\small easy sheaf condition} 
    \end{array}
  \end{tikzcd}
\]
\begin{defn}
  We say that a topological space $X$ is \emph{compactly generated} if it satisfies the following condition:
    For any subspace $A \sse X$, it holds that $A$ is closed in $X$ if and only if $A\cap K$ is closed in $K$ for any compact space $K$.
\end{defn}

\begin{prop}[\cite{refcondensed}*{Prop. 1.7}]
  \label{image-uline}
  The assignement $X \mapsto \underline{X}$ from \cref{top-to-condensed} is faithful, and fully faithful when restricted to the full subcategory of compactly generated topological spaces.
  The functor $(-)\mc \topcat \to \condset$ admits a left-adjoint $T \mapsto T(\ast)_{\mathrm{top}}$, where the underlying set of the topological space $T(\ast)_{\mathrm{top}}$ is defined as $\Gamma(\ast,T)$ and the topology is the quotient topology for the natural map\footnote{I think this one of the instances where set-theoretical bounds are needed to make sense of the coproduct on the left.} 
  \[
    \begin{tikzcd}[cramped, column sep = large]
    \displaystyle
    \bigsqcup_{\substack{\text{$S$ profinite}\\\text{$f_S\in \Gamma(S,T)$}}}
    S
    \ar{r}[hookrightarrow]{\sqcup f_S}
    &
    \Gamma(\ast,T)
    \end{tikzcd}.
  \]
\end{prop}
\subsection{Cohomology of compact Hausdorff spaces}
\begin{numtext}
  \label{cohomology-intro}
We now turn towards the cohomology of a condensed abelian group.
To start off, we are interested in the several classical ways we can describe ``an integral cohomology'' associated to a locally compact space $S$:
\begin{enumerate}
  \item
    Since $S$ is a (mere) topological space after all, we can consider the singular cohomology groups $\hh^i_{\mathrm{sing}}(S,\zz)$ from usual topology.
    However, this is only well-behaved if the space itself is well-behaved (from the point of view of algebraic topology, so for example a CW-complex).
    In our setup, the problem is that for any profinite $S$, we have that $\hh^0_{\mathrm{sing}}(S,\zz) = \maps(S,\zz)$, i.e. singular cohomology completely ignores the topology on $S$.\footnote{We also have that $\hh^i_{\mathrm{sing}}(S,\zz) = 0$ for $i\geq 0$; in the lecture, it was stated that singular cohomology always commutes with cofiltered limits, but this is actually not true (a counterexample should be given by any space for which singular and \v{C}ech cohomology do not coincide). I think the argument for profinite sets is much simpler, since already from the point of view of singular chains, any profinite set is just a point (aka $\contmap(\Delta^n,S) = \lset \ast \rset$ for $n\geq 1$). Note that the same vanishing result is true for profinite sets when regarded as objects of $\ast\pets$ and we're taking sheaf cohomology. Anyways, the point is that we won't work with singular cohomology, so this footnote is just a big old red hering.}
  \item 
    \label{cech-of-profinite}
    We can also take \v{C}ech cohomology (with integral coefficients) of $S$ considered as a topological space.
    This has the advantage that \v{C}ech cohomology of compact-Hausdorff spaces always commutes with cofiltered limits --- in particular, we have $\chh^i(S,\zz) = 0$ for $i\geq 1$.
    \v{C}ech cohomology is also naturally isomorphic to the sheaf cohomology of the constant sheaf $\zz$ on the topological space $S$.
\end{enumerate}
\end{numtext}
\begin{numtext}
  A third approach is offered by the general notion of sheaf cohomology on a site.
  For this to work, we need that $\condab$ is well-behaved --- in fact, it is ``as well-behaved as it can possibly be'' (c.f. \cite{refcondensed}*{Thm. 2.2}).
  In particular, $\condab$ is generated by compact projective objects, and so we can make sense of its derived category $D\condab$.\footnote{As an aside, we remark that $\condab$ does \emph{not} have enough injective objects \cite{scholze-injectives} and that this is one of the instances where differentiating between condensed abelian groups and $\kappa$-small condensed abelian groups for a strongly inaccessible cardinal $\kappa$ makes all the difference} 
  We can explicitly describe a set of compact projective generators of $\condab$:
  \begin{inprop}
    The forgetful functor $\condset \to \condab$ admits a left-adjoint, which we denote as 
    \[
      \zz[-]
      \mc 
      \condset 
      \to 
      \condab,
      ~
      T 
      \mapsto 
      \zz[T].
    \]
    The collection
    \[
      \lset 
      \zz[S]
      \ssp 
      \text{$S$ extremally disconnected}
      \rset
    \]
    is a set of compact projective generators of $\condab$.
  \end{inprop}
  The proof of this proposition is part of the proof of Theorem 2.2 in \cite{refcondensed}. 
  With this out of the way, we can define 
  \[
    \hh^i\cindex(S,\zz)
    \defined 
    \ext^i_{\condab}(\zz[S],\underline{\zz}),
  \]
  for any compact-Hausdorff space $S$.
  Note that this is equivalent to 
  \[
    \hh^i\left(\left(\ast^{\mathrm{CHaus}}\pets\right)_{/S},\underline{\zz}_{/S}\right),
  \]
  which is the cohomology of the sheaf $\underline{\zz}_{/S}$ on the site $\left(\ast^{\mathrm{CHaus}}\pets\right)_{/S}$.
  While the latter definition is more intrinsic to the site we're working with, the definition involving $\ext$-groups has the advantage that it tells us that we can compute the cohomology groups $\hh^i_{\mathrm{cond}}(S,\zz)$ for any compact-Hausdorff space $S$ by ``taking projective resolutions'':
  \begin{inlem}
    \begin{enumerate}
      \item 
        We construct a ``simplicial hypercover'' $S_{\bullet} \to S$ by extremally disconnected spaces roughly as follows:
        Let $S_0 \twoheadrightarrow S$ be a surjection (as in \cref{extdisc-resol}).
        Choose now a surjection $S_1 \twoheadrightarrow S_0 \times_S S_0$, again with $S_1$ extremally disconnected.
        For the construction of $S_2$, consider the maps 
        \[
          d_{1,2}\mc 
          \begin{tikzcd}[cramped, column sep = small]
            S_1
            \ar[twoheadrightarrow, r]
            &
            S_0 \times_S S_0
            \ar{r}[above]{\projection_{1,2}}
            &
            [0.9em]
            S.
          \end{tikzcd}
        \]
          We then choose a surjection
          \[
            \begin{tikzcd}[cramped,ampersand replacement=\&]
            S_2
            \ar[twoheadrightarrow]{r}
            \&
            \lset 
            (u,v,w)
            \in 
            S_1
            \times 
            S_1
            \times 
            S_1
            \ssp 
            \arraycolsep=1.5pt
            \begin{array}{lll}
              d_1(u) &=& d_1(v)
              \\
              d_2(u) &=& d_1(w)
              \\
              d_2(v) &=& d_2(w)
            \end{array}
          \rset.
          \end{tikzcd}
        \]
      \item
        
    \end{enumerate}
  \end{inlem}
  Finally let us remark that the above definitions and considerations did not exploit any of the special properties of $\zz$, so they remain valid for any \emph{discrete} abelian group in place of $\zz$.
  To avoid confusion, we quickly spell out the definition of these cohomology groups, namely, we set
  \[
    \hh^i\cindex(S,M) 
    \defined 
    \ext^i_{\condab}(\zz[S],\underline{M}),
  \]
  the point being that $M$ only replaces $\zz$ \emph{in the second entry} of $\ext$.
\end{numtext}

\begin{theorem}
  \label{cohomology-of-profinite}
  Let $S$ be a compact-Hausdorff space and $M$ a discrete abelian group.
  Then there are natural isomorphisms 
  \[
    \hh^i_{\mathrm{Sheaf}}(S,M)
    \cong 
    \hh^i\cindex(S,M).
  \]
\end{theorem}
\begin{inrem}
In particular, we get that if $S$ is profinite, then it holds that 
\[
    \hh^i\cindex(S,M) 
    =
    \begin{cases}
      \contmap(S,M),
      &
      \text{if $i=0$;}
      \\
      0,
      &
      \text{otherwise.}
    \end{cases}
\]
Also, we note that the results change if $M$ is no longer discrete (for example, if $M = \rr$ with its natural topology, c.f. \cref{cond-coh-with-real-coeff} below).
\end{inrem}
\begin{proof}
 We will do this in three steps, first treating the case that $S$ is extremally disconnected, then that it is profinite and finally the general case.
 \begin{enumerate}
   \item 
     Assume first that $S$ is extremally disconnected.
     Then $S$ is a projective object in $\condset$, and so for \emph{any} sheaf of abelian groups $\shm \in \condset_{/S}$, it holds that 
     \[
       \hh^i\cindex(S,\shm)
       = 
       \begin{cases}
         \Gamma(S,\shm),
         &
         \text{if $i = 0$;}
        \\
        0,
        &
        \text{otherwise.}
      \end{cases}
      \]
      Since also $\constfunct(S,M) = \locconstfunct(S,M)$ holds, this also agress with the sheaf cohomology of $M$ on the topological space $S$.
    \item
      Consider now the case that $S$ is profinite.
      We already know that $\hh^i_{\mathrm{Sheaf}}(S,M) = 0$ for $i \geq 1$ (\cref{cohomology-intro}, \ref{cech-of-profinite}).
      To get the same result for $\hh^i\cindex$, we first choose a simplicial hypercover $S_{\bullet} \to S$, such that all the $S_i$ are extremally disconnected; we then have to show that the complex 
      \begin{equation}
        \tag{$\ast$}
        \label{big-hypercover}
        \begin{tikzcd}[column sep = small]
          0
          \ar{r}
          &
          M(S)
          \ar{r}
          &
          M(S_0)
          \ar{r}
          &
          M(S_1)
          \ar{r}
          &
          \ldots
        \end{tikzcd}
      \end{equation}
      is exact.
      Now each of the $S_i$ itself admits a hypercover $S_{i,\bullet}$ by extremally disconnected sets $S_{i,j}$, and these can be choosen in such a way that $S_{\bullet} = \varprojlim_{i} S_{i,\bullet}$ holds.
      In that case, the complex \eqref{big-hypercover} can be written as a filtered colimit of the complexes \eqref{i-th-hypercover}, that are given by 
      \begin{equation}
        \tag{$\ast_i$}
        \label{i-th-hypercover}
        \begin{tikzcd}[column sep = small]
          0
          \ar{r}
          &
          M(S_i)
          \ar{r}
          &
          M(S_{i,0})
          \ar{r}
          &
          M(S_{i,1})
          \ar{r}
          &
          \ldots
        \end{tikzcd},
      \end{equation}
      and since taking filtered colimits preserves exactness, it suffices to show that each of the \eqref{i-th-hypercover} is exact.
      But this is now automatic, since each of the hypercovers $S_{i,\bullet} \to S_i$ splits (because all of the $S_i$ are extremally disconnected).
      Then this splitting can be used to give a contracting homotopy of each of the \eqref{i-th-hypercover}. 
    \item 
      \coms 
        \emph{next lecture}
      \come
  \end{enumerate}
\end{proof}

\begin{theorem}
  \label{cond-coh-with-real-coeff}
  Let $S$ be a compact Hausdorff space, and consider $\rr\in \condab$ with its natural topology.
  Then 
  \[
    \hh^i\cindex(S,\rr) = 0
  \]
  for $i\geq 1$, while $\hh^0\cindex(S,\rr) = C(S,\rr)$ is the space of continous real-valued functions on $S$.
\end{theorem}
