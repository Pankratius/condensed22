\subsection{More on $\condab$}

\begin{lem}
  \label{tensor-on-ed}
  Let $M,N\in \condab$ be condensed abelian groups.
  Then for any extremally disconnected set $S$, it holds that 
  \[
    (M\tensor N)(S) = M(S) \tensor N(S)
  \]
\end{lem}
\begin{proof}
  This holds since tensor products of abelian groups commute with finite products (in each variable), and because this is the only sheaf condition we need to check on $\ast\pets^{\mathrm{ED}}$.
\end{proof}

\subsection{Locally compact abelian groups}
  We now turn towards a class of abelian groups where we can explicit describe all $\rderived\chom$-complexes in $D(\condab)$.
  To warm up, we recall that since $\zz$ has cohomological dimension 1, we have $\ext^i(A,B) = 0$ for all discrete abelian groups $A,B$ and $i \geq 2$.
\begin{numtext}
  As it turns out, we can single out a huge class of topological abelian groups $A$ whose condensation $\underline{A}$ satisfies a similar relation, even for the $\cext$-sheaves.
  This will be the collection of \emph{locally compact abelian groups}, i.e. topological groups $A$ in which any point $x\in A$ has a neighborhood basis of compact Hausdorff neighborhoods.
  Next to discrete groups, we also have that $\rationals_p$ and $\rr$ are locally compact, as well as finite-dimensional Hausdorff spaces over them.
  Furthermore, we have 
  \begin{inlem}
    Let $A$ be a locally compact abelian group.
    If $B \sse A$ is a closed subgroup, then $B$ is locally compact.
    Any topological quotient of $A$ is locally compact.
    If $I$ is an index set, and $(A_i)_{i\in I}$ a collection of locally compact abelian groups, then $\prod_{i\in I} A_i$ is locally compact if and only if all but finitely many $A_i$ are actually compact.
  \end{inlem}
  We will also make use of the following structural result for locally compact abelian groups:
  \begin{inthm}
    \label{lca-structure}
    \begin{enumerate}
      \item 
        Let $A$ be a locally compact abelian group.
        Then there is an integer $n$ and an isomorphism $A \cong \rr^n \times A'$, where $A'$ is an extension of a discrete abelian group by a compact abelian group.
      \item 
        Denote by $\cgroup \defined \rr/\zz$ the circle group.
        Then the assignement $A \mapsto \hom^{\mathrm{c.o.}}(A,\cgroup)$ takes vaues in locally compact abelian groups, and introduces a contravariant autoduality on the category of locally compact abelian groups, which we denote as 
        \[
          \dd(-)\mc 
          \lcacat\op 
          \isomorphism 
          \lcacat,
          ~
          A \longmapsto \dd(A),
        \]
        and refer to as \emph{Pontrjagin duality}.
        The natural map $A \to \dd(\dd(A))$ is an isomorphism.
      \item
        The Pontrjagin duality functor $\dd(-)$ restricts to a contravariant duallity between compact abelian groups and discrete groups.
    \end{enumerate}
  \end{inthm}
  \begin{inexample}
    \begin{enumerate}
      \item
        The Pontrjagin dual of $\cgroup$ is given by $\zz$ (where this $\zz$ comes from the fundamental group of $S^1$).
        Note that in general, locally compact are not cartesian-closed, i.e. for $X,Y$ both locally compact, the mapping space $\hom^{\mathrm{c.o.}}(X,Y)$ with the compact-open topology is not necessarily locally compact again.
        As a simple example for that, we note that if $I$ is a discrete space and $Y$ any topological space, then 
        \[
          \hom^{\mathrm{c.o.}}(I,Y)
          \cong 
          \prod_{\abs{I}} Y,
        \]
        where for the moment, we write $\abs{I}$ for the set underlying the space $I$.
        But if $Y$ is only locally compact but not compact, and $I$ uncountable, then this product is no longer locally compact.
      \item 
        The category of locally compact is still not abelian (our prime example of $\id\mc \rr^{\mathrm{disc}} \to \rr$ is a continous map of locally compact abelian groups after all).
        However, there is a notion of ``a derived category of locally compact abelian groups'' (doing homological algebra with locally compact abelian groups was apparently done already in the 60's (c.f. \cite{moskowitz}); Clausen-Scholze cite \cite{hs07}).
      \end{enumerate}
  \end{inexample}
  We also have the following relation between locally compact topological spaces and compactly generated spaces (which I just shamelessly copied from the nLab...):
  \begin{inlem}
    \begin{enumerate}
      \item 
        Every locally compact Hausdorff space is compactly generated.
      \item 
        A topological space is compactly generated if and only if it is the quotient of a locally compact Hausdorff space.
    \end{enumerate}
  \end{inlem}
\end{numtext}

\begin{prop}
  \label{lca-hom-compare}
  Let $A$ and $B$ be Hausdorff, compactly generated topological abelian groups.
  Then there is a natural isomorphism of condensed abelian groups: 
  \[
    \chom(\underline{A},\underline{B})
    \isomorphism 
    \underline{\hom^{\mathrm{c.o.}}(A,B)}
  \]
\end{prop}

\begin{proof}
  Let $S$ be a profinite set.
  Then the $S$-valued points of the left-hand side can be described as 
  \begin{align*}
    \chom(\underline{A},\underline{B})(S)
    &=
    \hom_{\condset}(\underline{S},\chom(\underline{A},\underline{B}))
    \\
    &=
    \hom_{\condab}(\zz[S],\chom(\underline{A},\underline{B}))
    \\
    &=
    \hom_{\condab}(\zz[S]\tensor\underline{A},\underline{B}).
  \end{align*}
  Consider now the surjection of condensed abelian groups 
  \[
    \begin{tikzcd}[column sep = small, cramped]
      \zz[S]
      \tensor 
      \zz[A]
      \ar[twoheadrightarrow]{r}
      &
      \zz[S]
      \tensor 
      \underline{A}
    \end{tikzcd}~,
  \]
  where we regard $A$ ``only'' as a locally compact abelian group on the left-hand side (we will take the group structure into account later).
  Using the identifications $\zz[S\times A] = \zz[S] \tensor \zz[A]$ and 
  \[
    \hom_{\condab}(\zz[S]\tensor \zz[A], B)
    \cong 
    \underline{\contmap(A,B)}(S)
  \]
  and the right-exactness of the (contravariant) $\hom$-functor, this yield an injection of abelian groups
  \[
    \begin{tikzcd}[column sep = small, cramped]
      \Phi_S
      \mc 
      \hom(\zz[S]\tensor A,B)
      \ar[hookrightarrow]{r}
      &
      \hom(\zz[S]\tensor \zz[A],B)
      \ar{r}[above]{\sim}
      &
      \underline{\contmap(A,B)}(S)
    \end{tikzcd}
    ~,
  \]
  for every topological abelian group $B$.
  Explicitly, this is given as follows:
  Let $f\in \chom(\underline{A},\underline{B})(S)$ be an element, which we think of as a map of condensed abelian groups 
  \[
    f\mc \zz[S] \tensor \underline{A} 
    \longrightarrow
    \underline{B};
  \]
  evaluating this at the point gives a map of abelian groups
  \[
    f_{(\ast)}
    \mc
    \zz[S] \tensor_{\zz}
    A
    \longrightarrow 
    B,
  \]
  which in turn induces a map 
  \begin{align*}
    S
    &
    \longrightarrow 
    \hom^{\mathrm{c.o.}}(A,B)
    \\
    s 
    &
    \longmapsto
    \left[
      A \to B; 
      a \mapsto f_{(\ast)}(s\tensor a)
    \right].
  \end{align*}
  This is the desired element $\Phi_{S}(f)$.
  In particular, we see that $\Phi$ factors over $\underline{\hom^{\mathrm{c.o.}}(A,B)}$.
  We now show that this factorization is onto, i.e. that for every profinite set $S$ and every morphism of condensed abelian groups $f\mc \zz[S] \tensor \zz[A] \to \underline{B}$ there exists a factorization of the form: 
  \begin{equation}
    \label{lca-factorization1}
    \begin{tikzcd}
      \zz[\underline{S}]
      \tensor 
      \zz[A]
      \ar{r}[above]{f}
      \ar[twoheadrightarrow]{d}[left]{\pi}
      &
      \underline{B}
      \\
      \zz[\underline{S}]
      \tensor{A}
      \ar[dashed]{ur}[below right]{\exists ?}
    \end{tikzcd}
  \end{equation}
  For this, we use the following partial resolution of the condensed abelian group $\underline{A}$:
  \begin{equation}
    \label{2term-resolution}
    \begin{tikzcd}[column sep = small, cramped]
      \zz[A]
      \tensor
      \zz[A]
      \ar{r}
      &
      \zz[A]
      \ar{r}
      &
      A
      \ar{r}
      &
      0
    \end{tikzcd},
  \end{equation}
  where the first map is induced by the map of abelian groups 
  \begin{align*}
    \zz[A] \tensor_\zz \zz[A]
    &
    \longrightarrow 
    \zz[A]
    \\
    [a_1] \tensor [a_2]
    &
    \longmapsto 
    [a_1+a_2]
    -
    [a_1]
    -
    [a_2].
  \end{align*}
  Since $\zz[S]$ is exact (it's free), we get that the map $\pi \mc \zz[S] \tensor \zz[A] \to \zz[S] \tensor A$ is a cokernel for 
  \[\zz[\underline{S}]\tensor \zz[A] \tensor \zz[A] \to \zz[S] \tensor \zz[A];\]
  i.e showing the existence of a factorization in \eqref{lca-factorization1} is equivalent to showing that the composition 
  \begin{equation}
    \label{lca-composition}
    \begin{tikzcd}[column sep = small, cramped]
      \zz[S]\tensor \zz[A] \tensor \zz[A]
      \ar{r}
      &
      \zz[S]
      \tensor 
      \zz[A]
      \ar{r}[above]{f}
      &
      \underline{B}
    \end{tikzcd}
  \end{equation}
  vanishes.
  \coms Now people say that this is equivalet to a morphism of condensend sets $\underline{S} \times \underline{A} \times \underline{A} \to \underline{B}$, which in fact comes from a morphism of topological spaces $S \times A \times A \to B$ (by the \emph{equivalence} part of \cref{image-uline}) and now that ``This map is trivial as for all $s\in S$, the map $A \to B$ parametrized by $s$ is a group homomorphism.'' 
  I have a hard time understanding how to turn this intuition into a precise argument, and I also don't see how this works for \emph{arbitrary} $B$ (as it is stated in \cite{refcondensed}*{4.2}).
  The following is something I've scrambled together...
  \ruat
  By the adjunction part of \cref{image-uline}, the map of condensed sets \eqref{lca-composition} is equivalent to a map of topological spaces 
  \[
    \begin{tikzcd}[column sep = large, ampersand replacement=\&]
    \zz[S]
    \tensor_{\zz}
    \zz[A]
    \tensor_{\zz}
    \zz[A]
    \ar{rr}[above]{\begin{array}{c} s\tensor[a]\tensor [a'] \\ \text{\rotatebox[origin = c]{270}{$\mapsto$}} \\ s \tensor ([a+a'] - [a] - [a'])\end{array}}
    \&
    \&
    \zz[S]
    \tensor_{\zz}
    A
    \ar{r}[above]{f}
    \&
    B
    \end{tikzcd}
  \]
  Now the point is for every $s\in S$, the morphism 
  \[
    A\to B,~ a\mapsto f(s\tensor a)
  \]
  is again a group homomorphism, which implies that the above composition is indeed zero.
  \come
\end{proof} 

\begin{numtext}
  Before we proceed with the main calculations involving locally compact abelian groups, we give a brief outline of some of some properties of the ``bounded-derived category of locally compact abelian groups'' as in \cite{hs07}.
  \begin{indefn}
    Let $f\mc A \to B$ be a morphism of LCAs.
    We say that $f$ is \emph{strict}, if the induced isomorphism of abelian groups
    \[
      A/\ker(f)
      \isomorphism
      \im(f)
    \]
    is a homeomorphism, where $A/\ker(f)$ is induced with the quotient topology and $\im(f)\sse B$ is endowed with the subspace topology.
    If $A_{\bullet}$ is a complex of LCAs.
    Then $A_{\bullet}$ is called strict if every differential $d\mc A \to A$ of $A_{\bullet}$ is a strict morphism of LCAs.
    Finally, a morphism $f\mc A_{\bullet} \to B_{\bullet}$ of bounded complexes of LCAs is called a \emph{quasi-isomorphism} if its cone is exact and strict.
  \end{indefn}
  Strictness finally gets rid of some of the pathologies of topological abelian groups (for example, the bijection $\rr_{\mathrm{discr}} \to \rr_{\mathrm{nat}}$ is not strict).
  The category $\lcacat$ becomes then an ``exact category'' (with exact sequences given by strict-exact sequences), and we can localize it at the quasi-isomorphisms of LCAs, which leads to a category we denote by $D^b(\lcacat)$.
  We now want that the functor $\underline{(-)}\mc \lcacat \to \condab$ can be extended to derived categories.
  I'm not sure what exactly we need to check, but the crucial calculation should be the following:
  \begin{inlem}
    Let $\begin{tikzcd}[column sep = small, cramped] 0 \ar{r} & A\ar{r}& B \ar{r}& C\ar{r} & 0\end{tikzcd}$ be a strict-exact sequence of LCAs.
    Then the induced sequence $\begin{tikzcd}[column sep = small, cramped] 0 \ar{r} & \underline{A} \ar{r}& \underline{B} \ar{r}& \underline{C}\ar{r} & 0\end{tikzcd}$ of condensed abelian groups is again exact.
  \end{inlem}
  \begin{proof}
    The crucial part is to show it for surjections, i.e. that for any surjection $\pi \mc B \twoheadrightarrow C$ of LCAs and any morphism $f\mc S \to C$, with $S$ extremally disconnected, there exists a lift to $B$:
    \[
      \begin{tikzcd}
        &
        B
        \ar[twoheadrightarrow]{d}[right]{\pi}
        \\
        S
        \ar{r}[below]{f}
        \ar[dashed]{ur}[above left]{?}
        &
        C
      \end{tikzcd}
    \]
    The ``problem'' is that $C$ is only locally compact, so we cannot directly deduce the existence of the lift from the fact that $S$ is extremally disconnected.
    But we can work our way there:
    For every $x\in f(S)$, there is a $y\in B$ with $\pi(y) = x$.
  Since $B$ is locally compact, there exists a compact neighborhood $K'_y\sse B$ of $y$; then $K_x \defined \pi(K'_y) \sse X$ is a compact neighborhood of $x \defined \pi(y)$ (note that $\pi$ is open, because it is strict).
  Because $S$ is extremally disconnected, the image $f(S)$ is compact in $C$.
  So we can choose finitely many $x_1,\ldots,x_n \in f(S)$ with preimages $y_1,\ldots,y_n \in Y$ such that for $K \bigcup_{i=1}^n K_{x_i} \sse X$ and $K' \defined \bigcup_{i=1}^n K'_{y_i} \sse Y$ it holds that both $K,K'$ are compact-Hausdorff, $f(S) \sse K$ and the restricted map $\restrict{\pi}{K'}\mc K' \to K$ is still surjective.
  Hence we obtain a factorization of the form 
  \[
    \begin{tikzcd}[column sep = small]
    &
    &
    K'
    \ar[twoheadrightarrow]{d}
    \\
    S
    \ar{r}[below]{f}
    \ar[dashed, bend left = 20]{urr}
    &
    \im(f)
    \ar[hookrightarrow]{r}
    &
    K
  \end{tikzcd}
\]
  which is all we need to establish the factorization we were intially after.
  \end{proof}
\end{numtext}


\begin{theorem}
  \label{iso-on-rhom}
  Let $A,B$ be locally compact abelian groups.
  Then the natural morphism 
  \[
    \rhom_{\lcacat}(A,B)
    \longrightarrow
    \rhom_{\condab}(\underline{A},\underline{B})
  \]
  is an isomorphism in $D(\abcat)$.
\end{theorem}
Our strategy for proving this theorem is as follows: we can calculate the $\rchom$ between arbitrary products of $\ctt$ and discrete groups or $\rr$.
The crucial ingredient for this will be a resulotion of discrete abelian groups that vastly generalizes the one we used in the proof of \cref{lca-hom-compare}.
We can then use a spectral sequence argument and the classification of LCAs to conclude the claimed isomoprhism by comparing the consequences of \cref{structure-in-dcab} with the results obtained in \cite{hs07}.
\begin{inthm}
  \label{structure-in-dcab}
  Let $ A \defined \prod_{I}\ctt$, where $I$ is any (possibly infinite) index set.
  \begin{enumerate}
    \item
      \label{structure-in-dcab:discrete}
    For any discrete abelian group $M$, it holds that 
    \[
      \rchom(A,M)
      =
      \bigoplus_{I}M[-1],
    \]
    where the map $\bigoplus_I M[-1] \to \rchom(A,M)$ is induced by the $I$-indexed collection of maps 
    \[
      \begin{tikzcd}[column sep = small, cramped]
      M[-1]
      =
      \rchom(\zz[1],M)
      \ar{r}
      &
      \rchom(\ctt, M)
      \ar{r}[above]{p_i^{\ast}}
      &
      \rchom(\displaystyle \prod_I \ctt, M)
      =
      \rchom(A,M)
      \end{tikzcd},
    \]
    where $p_i^{\ast}$ is the pulbback under the projection $p_i\mc \prod_I \ctt \to \ctt$ to the $i$-th factor.
  \item 
    \label{structure-in-dcab:real}
    It holds that $\rchom(A,\rr) = 0$.
  \end{enumerate}
\end{inthm}

\begin{innumtext}[Reducing \ref{iso-on-rhom} to \ref{structure-in-dcab}]
  By the classification from \cref{lca-structure}, it suffices to consider the cases that either (i) $A$ is discrete, (ii) $A$ is compact or (iii) $A = \rr$.
  \begin{enumerate}
    \item
      If $A$ is discrete, then we have a (possibly very large) resolution of the form 
      \[
        \begin{tikzcd}[column sep = small, cramped]
          0
          \ar{r}
          &
          \displaystyle 
          \bigoplus_J
          \zz
          \ar{r}
          &
          \displaystyle
          \bigoplus_I
          \zz
          \ar{r}
          &
          A
          \ar{r}
          &
          0.
        \end{tikzcd}
      \]
      Because $\rchom(-,B)$ commutes with filtered colimits, we can assume $A = \zz$.
      In that case, the assertion of \cref{iso-on-rhom} is \coms clear \come.
    \item 
      If $A$ is compact, then the Pontrjagin-dual $D(A)$ is discrete, and so we obtain again a resultion as above.
      Applying the Pontrjagin-dual a second time, we thus obtain a ``contravariant resultion'' of the form 
      \[
        \begin{tikzcd}[column sep = small, cramped]
          0
          \ar{r}
          &
          A
          \ar{r}
          &
          \displaystyle
          \prod_{I'}
          \ctt
          \ar{r}
          &
          \displaystyle
          \prod_{J'}
          \ctt
          \ar{r}
          &
          0,
        \end{tikzcd}
      \]
      This is a situation in which we can use \cref{structure-in-dcab}.
    \item
      Finally, in the case $A = \rr$, we can use the short-exact sequence 
      \[
        \begin{tikzcd}[column sep = small, cramped]
          0
          \ar{r}
          &
          \zz 
          \ar{r}
          &
          \rr
          \ar{r}
          &
          \ctt
          \ar{r}
          &
          0
        \end{tikzcd},
      \]
      to deduce the statement for $A$ from the previous two cases.
  \end{enumerate}
  We can also reduce the case that $B$ is compact to the case that $B$ is either discrete or $B = \rr$:
  Namely, if $B$ is compact, we can again suppose $B = \prod \ctt$, and in that case, $B$ sits in an exact sequence of the form 
  \[
        \begin{tikzcd}[column sep = small, cramped]
          0
          \ar{r}
          &
          \displaystyle
          \prod_{I''}
          \zz
          \ar{r}
          &
          \displaystyle
          \prod_{J''}
          \rr
          \ar{r}
          &
          B
          \ar{r}
          &
          0
        \end{tikzcd},
    \]
    because arbitrary products in $\condab$ are exacts.
    The claim that it suffices to consider the cases $B=\rr$ or $B$ discrete then follows from the fact that $\rchom(A,-)$ commutes with arbitrary products.
    So we're again in the setup of \cref{structure-in-dcab}
    \par 
    Finally, \cref{iso-on-rhom} follows from \cref{structure-in-dcab}, because \cite{hs07} calculated the various $\ext$-groups in theses cases for $A$ and $B$ and get results that agree with the ones we're claiming.
\end{innumtext}


\begin{inthm}[Eilenberg-MacLane, Breen, Deligne, Clausen-Scholze]
  Let $A$ be any (discrete) abelian group.
  Then there is a functorial resolution of $A$ of the form 
  \begin{equation}
    \label{bd-res}
    \tag{BD}
    \begin{tikzcd}[column sep = small, cramped]
      \ldots 
      \ar{r}
      &
      \displaystyle
      \bigoplus_{j = 1}^{n_i}
      \zz[A^{r_{i,j}}]
      \ar{r}
      &
      \ldots
      \ar{r}
      &
      \zz[A^3]
      \oplus
      \zz[A^2]
      \ar{r}
      &
      \zz[A^2]
      \ar{r}
      &
      \zz[A]
      \ar{r}
      &
      A
      \ar{r}
      &
      0,
    \end{tikzcd}
  \end{equation}
    where all $n_i$ and $r_{i,j}$ are non-negative integers, that are independent of the group $A$.
    A similar resolution exists for any condensed abelian group, with each of the $\zz[A^{r_{i,j}}]$ replaced by the sheafification of the assignment $S \mapsto \zz[A^{r_{i,j}}(S)]$.
\end{inthm}
This theorem seems to have a longer history, but a proof apparently only appeared in \cite{refcondensed}.
We will not attempt to reproduce their proof, since it does not directly relate to the other topics we intend to cover.
The application of this resolution that we are interested in is the following:
\begin{incor}
  Let $A,M$ be condensed abelian groups and $S$ an extremally disconnected set.
  Then there is a spectral sequence
  \[
    E_1^{p,q}
    =
    \prod_{j = 1}^{n_p}
    \hh^q(A^{r_{p,j}} \times S, M)
    \Longrightarrow
    \cext^{p+q}(A,M)(S),
  \]
  where the coefficients $n_p, r_{p,j}$ are the ones appearing in \eqref{bd-res}.
  This resolution is functorial in $A,M$ and $S$.
\end{incor}

\begin{proof}
  Because $\zz[S]$ is a free condensed abelian group, we can tensor \eqref{bd-res} with it to obtain a resolution of the form 
  \[
    \begin{tikzcd}[column sep = small, cramped]
      \ldots 
      \ar{r}
      &
      \displaystyle
      \bigoplus_{j = 1}^{n_i}
      \zz[A^{r_{i,j}} \times S]
      \ar{r}
      &
      \ldots
      \ar{r}
      &
      \zz[A^3 \times S]
      \oplus
      \zz[A^2 \times S]
      \ar{r}
      &
      \zz[A^2 \times S]
      \ar{r}
      &
      \zz[A]
      \ar{r}
      &
      A \tensor \zz[S]
      \ar{r}
      &
      0,
    \end{tikzcd}
  \]
  which we denote by $\shf^{\bullet}$.
  By the general theory of derived categories of abelian categories, there is then a functorial spectral sequence of the form 
  \[
    E_1^{p,q}
    =
    \ext^q(\shf^p, M)
    \Longrightarrow
    \ext^{p+q}(\shf^{\bullet},M).
  \]
  We now identify either side of this with the respective counterpart of the claim:
  For the left-hand side, we have 
  \begin{align*}
    \ext^q(\shf^p,M)
    &
    =
    \ext^q(\bigoplus_{j=1}^p \zz[A^{r_{p,j}} \times S], M)
    \\
    &
    =
    \bigoplus_{j=1}^p \ext^q(\zz[A^{r_{p,j}} \times S], M))
    \\
    &
    =
    \bigoplus_{j = 1}^p \hh^q(A^{r_{p,j}}\times S, M);
  \intertext{and for the right-hand side, we have}
    \ext^{p+q}(\shf^{\bullet},M)
    &=
    \ext^{p+q}(A\tensor \zz[S],M)
    \\
    &
    =
    \cext^{p+q}(A,M)(S),
  \end{align*}
  where the last-equality needs that $S$ is extremally disconnected to infer the quasi-isomorphism of complexes of abelian groups
  \[
    \rhom(A\tensor \zz[S], M)
    \simeq
    \rchom(A,M)(S).
  \]
\end{proof}

\begin{proof}[Proof of \cref{structure-in-dcab}]
 We first start with part \ref{structure-in-dcab:discrete} (the statement about discrete groups).
 Assume first that $I$ is finite; since $\rchom$ commutes with direct sums in the first entry, we can assume that $I$ contains one element, and are left with calculating $\rchom(\cgroup, M)$ for any discrete group.
  The claim now is that the map 
  \[
    M[-1]
    =
    \rchom(\zz[1],M)
    \to 
    \rchom(\cgroup, M)
  \]
  is an isomorphism in $D(\condab)$.
  Using the long-exact sequence associated to 
  \[
    \begin{tikzcd}[column sep  = small, cramped]
      0
      \ar{r}
      &
      \zz
      \ar{r}
      &
      \rr
      \ar{r}
      &
      \cgroup
      \ar{r}
      &
      0
    \end{tikzcd},
  \]
  we can see that this is equivalent to showing $\rchom(\rr,M) = 0$; which in turn is the same as $0 \to \rr$ becoming a quasi-isomorphism after applying $\rchom(-,M)$.
\end{proof}
