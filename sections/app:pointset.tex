\section{Technical lemma etc.}

\subsection{Point-set topology}
\begin{lem}
  \label{app:tdisc-iff-tsep}
  Let $X$ be a locally compact topological space.
  Then $X$ is totally disconnected if and only if it is totally seperated.
\end{lem}
\begin{defn}
  Let $f\mc X \twoheadrightarrow Y$ be a surjective continous map of topological spaces.
  We say that $f$ is a \emph{quotient map} if for all subsets $U \sse Y$, it holds that $U$ is open in Y if and only if $f^{-1}(U)$ is open in $X$.
\end{defn}

\begin{lem}
  \label{app:surjective-chaus-quotient}
  Let $f\mc X\twoheadrightarrow Y$ be a surjective continous map and assume that $X$ is compact and that $Y$ is Hausdorff.
  Then $f$ is a quotient map.
\end{lem}

\subsubsection{Stone-\v{C}ech compactification}

\begin{lem}
  \label{app:sc-homeo-onto-image}
  Let $X$ be a topological space, and let $\beta X$ be its Stone-\v{C}ech compactification.
  Then the canonical map $X\to \beta X$ is a homoeomorphism onto its image if and only if $X$ is a Tychnoff-space.
\end{lem}

\subsection{Triangulated categories}

\begin{defn}
  Let $\tcat$ be a triangulated category and $\ncat \sse \tcat$ a full triangulated subcategory.
  The \emph{right-orthogonal} of $\ncat$ is the full subcategory $\orth{\ncat}$ consisting of all objects $M \in \tcat$ such that $\hom(N,M) = 0$ for all $N \in \ncat$.
  Analogously, one defines the \emph{left-orthogonal} $\orth*{\ncat}$ of $\ncat$.
\end{defn}

Note that $\orth{\tcat} = \orth*{\tcat} = 0$ holds for any triangulated category $\tcat$.

\begin{defn}
  Let $\tcat$ be a triangulated category.
  A \emph{semiorthogonal decomposition} of $\tcat$ is a sequence of full subcategories $\acat_1,\ldots,\acat_n$ in $\tcat$ such that $\hom_{\tcat}(\acat_i,\acat_j) = 0$ for $i>j$ and for every $T \in \tcat$ there exists a chain of morphisms 
  \[
    \begin{tikzcd}[column sep = small]
      0
      =
      T_n 
      \ar{r}
      &
      T_{n-1}
      \ar{r}
      &
      \ldots 
      \ar{r}
      &
      T_1
      \ar{r}
      &
      T_0 = T,
    \end{tikzcd}
  \]
  such that the cone of the morphism $T_k \to T_{k-1}$ is contained in $\acat_k$ for $k=1,2,\ldots,n$.
\end{defn}

\begin{defn}
  Let $\tcat$ be a triangulated category and let $\iota\mc \ncat \hookrightarrow \tcat$ be an embedding of a full triangulated subcategory $\ncat$.
  We say that $\ncat$ is \emph{right-admissible} if $\iota$ admits a right-adjoint functor $R \mc \tcat \to \ncat$.
  Similarly, we say that $\ncat$ is \emph{left-admissible} if $\iota$ admits a left-adjoint functor $L \mc \tcat \to \ncat$.
  We say that $\ncat$ is \emph{admissible} if it is both left- and right-admissible.
\end{defn}

\begin{lem}[\cite{stacks}*{\stackstag{0CQT}}]
  Let $\tcat$ be a triangulated category and $\ncat \sse \tcat$ a full triangulated subcategory.
  Then the following are equivalent:
  \begin{enumerate}
    \item 
      The inclusion functor $A \hookrightarrow \tcat$ has a left-adjoint;
    \item 
      For every $X \in \tcat$, there exists a distinguished triangle 
      \[
        \begin{tikzcd}[column sep = small]
          B 
          \ar{r}
          & 
          X 
          \ar{r}
          &
          A 
          \ar{r}
          &
          B[1]
        \end{tikzcd}
      \]
      in $\tcat$, with $A \in \ncat$ and $B \in \orth*{\ncat}$.
  \end{enumerate}
  If this holds, then $\ncat$ is saturated\footnote{We say that a subcategory $\ncat \sse \tcat$ is \emph{saturated} if $X \oplus Y$ being isomorphic to an element of $\ncat$ implies that both $X$ and $Y$ are isomorphic to objects in $\ncat$}.
  If $\ncat$ is moreover isomorphism-closed in $\tcat$, then $\ncat = \orth{(\orth*{\ncat})}$.
\end{lem}

\begin{lem}
  Let $\tcat$ be a triangulated category and let $\iota \mc \ncat \to \tcat$ be an embedding of a full triangulated subcategory.
  \begin{enumerate}
    \item 
      If $\ncat$ is right-admissible, then the Verdier quotient $\tcat/\ncat$ is equivalent to $\orth{\ncat}$.
      Similarly, if $\ncat$ is left-admissible, then the Verdier quotient $\tcat/\ncat$ is equivalent to $\orth*{\ncat}$.
    \item 
      Conversely, if the quotient functor $\tcat \to \tcat/\ncat$ admits a left-adjoint (respectively a right-adjoint), then $\tcat/\ncat \cong \orth{\ncat}$ (respectively $\tcat/\ncat \cong \orth*{\ncat}$).
  \end{enumerate}
\end{lem}
\begin{defn}
  An exact functor $\Phi \mc \bcat \to \acat$ of triangulated categories is called \emph{left-splitting} if $\ker \Phi$ is a left-admissible subcategory in $\bcat$, the restriction of $\Phi$ to $\orth*{(\ker \Phi)}$ is fully faithful and $\im \Phi$ is admissible in $\acat$.
\end{defn}

\begin{prop}[\cite{kuz-trig}*{Thm. 3.3}]
  \label{kuzprop}
  Let $\Phi\mc \bcat \to \acat$ be an exact functor of triangulated categories.
  Then the following are equivalent:
  \begin{enumerate}
    \item 
      $\Phi$ is left-splitting;
    \item 
      $\Phi$ has a left-adjoint $\Phi^{\ast}$ and the composition of the counit $\Phi^{\ast}\Phi \to \id_{\bcat}$ with $F$ induces an isomorphism $\Phi \Phi^{\ast} \Phi \cong \Phi$;
    \item 
      \label{kuzprop:dec}
      $\Phi$ has a left-adjoint $\Phi^{\ast}$, there are semiorthogonal decompositions 
      \[
        \bcat= \innerp{\ker \Phi}{\im \Phi^{\ast}},~
        \acat = \innerp{\im \Phi}{\ker \Phi^{\ast}},
      \]
      and the functors $\Phi$ and $\Phi^{\ast}$ induce quasi-inverse equivalences $\im \Phi^{\ast} \cong \im \Phi$;
    \item 
      There exists a triangulated category $\tcat$, and fully faithful functors $\alpha\mc \tcat \to \acat, \beta\mc \tcat \to \bcat$, such that $\alpha$ admits a left-adjoint, $\beta$ admits a right-adjoint $R_{\beta}$ and $\Phi \cong \alpha\circ R_{\beta}$ holds.
  \end{enumerate}
\end{prop}

\begin{lem}
  \label{trig:quotient}
  \cite{stacks}*{\stackstag{05RK}, \stackstag{05RJ}} (\coms using that in our case $\derivedd(A\ana)$ is saturated i guess \come).
\end{lem}

\subsection{Koszul complex}

\begin{construction}
  \leavevmode
  \begin{enumerate}
    \item Let $M$ be a module over a commutative ring $R$, and $\delta \mc M\to R$ be a $R$-linear map.
    Set
    \[
    \extp M \defined \bigoplus_{i\geq 0}\extp^i M
    =
    R \oplus M \oplus \extp^2 M \oplus \ldots
    \]
      Then $\delta$ induces an $R$-linear derivation $d$ on $\extp^{\bullet} M$, via the inclusion in the zeroth-summand:
    \[
    \begin{tikzcd}
      M
      \ar{r}[above]{\delta}
      \ar[hookrightarrow]{rd}
      &
      R
      \ar[hookrightarrow]{r}
      &
      \extp^{\bullet} M
      \\
      &
      \extp^{\bullet} M
      \ar[dashed]{ur}[below right]{\exists ! d}
      &
    \end{tikzcd}
    \]
    which is of the form
    \[
    d(e_1\wedge \ldots \wedge e_n)
    =
    \sum_{i=1}^n
    (-1)^{i+1} \delta(e_i)e_0\wedge \ldots \wedge \hat{e}_i\wedge \ldots \wedge e_n.
    \]
      One can calculate that this turns $\extp^{\bullet} M$ into a differential graded algebra, which we call the \emph{Koszul complex} and denote by $\koszul(M)$.\footnote{Our Koszul complex is homologically graded, one can alternatively define it cohomologically by inserting elements instead of omitting them, but the theory won't change}
    \item
      Let $x_{\bullet} \defined (x_1,\ldots,x_n)$ be an ordered sequence of elements in $R$.
    They define a form on $R^{\oplus n}$ with standard basis $b_i$:
    \[
    R^{\oplus n}\to R,~
    b_i\mapsto x_i.
    \]
      We can form the associated Koszul complex, which we call the Koszul complex $\koszul{x}$ of $x_{\bullet}$.
    \item We say that an ordered sequence $x_{\bullet}=(x_1,\ldots,x_n)$ of elements in $R$ is \emph{a regular sequence} if $x_i$ is not a zero-divisor in $R/(x_1,\ldots,x_{i-1})$.
  \end{enumerate}
\end{construction}
\begin{lem}[\cite{stacks}*{\stackstag{062F}}]
  \label{koszullem}
  If $x_{\bullet} = (x_1,\ldots,x_n)$ is a regular sequence, then $\koszul{x}$ is acyclic in positive degrees and $\hh_0(\koszul(x)) = R/\genby{x_{\bullet}}$.
  In particular, $\koszul(x)$ is a free resolution of $R$ of length $n$.
\end{lem}
