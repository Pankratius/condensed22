\section{Globalizations}

\subsection{Discrete Huber rings}

\begin{defn}
  A \emph{discrete Huber pair} is a pair $(A,A^+)$, where $A$ is a discrete ring and $A^+ \sse A$ is a subring.
\end{defn}

\begin{numtext}
  Let $(A,A^+)$ be a discrete Huber pair.
  We want to associate to it a topological space $X = \spa(A,A^+)$ together with a sheaves of rings $(\ox,\ox^+)$ such that (roughly) the assignement 
  \[(A,A^+) \mapsto (\spa(A,A^+),\ox,\ox^+)\]
  becomes fully faithful on suitable defined categories and globalizes to schemes.
  This will be done via valuations on $A$ (as they encode properness), so let us first make the following definition:
  \begin{indefn}
    Let $A$ be a ring and $\Gamma$ a (multiplicatively written) totally ordered abelian group.
    We say that a map $\abs{\cdot} \mc A\setminus \lset 0 \rset \to \Gamma$ is a \emph{valuation}, if for all $x,y\in A\setminus \lset 0 \rset$, the following conditions are satisfied:
    \[
      \abs{1} = 1,~\abs{xy} = \abs{x}\abs{y}, ~ \abs{x+y} \leq \max(\abs{x},\abs{y}). 
    \]
    Assume now that $\Gamma$ is augmented by a point $0$, then we order $\Gamma \cup \lset 0 \rset$ by requiring that $0< \gamma$ holds for all $\gamma \in \Gamma$.
    We then say that $\abs{\cdot}\mc A \to \Gamma \cup \lset 0 \rset$ is a \emph{valuation} if $\abs{0} = 0$ holds and the above identities hold when extended to zero (where we use the convention $0\gamma  = 0$ for all $\gamma \in \Gamma$).
    Two valutions $\abs{\cdot} A \to \Gamma \cup \lset 0 \rset$ and $\abs{\cdot}'\mc A \to \Gamma' \cup \lset 0 \rset$ are said to be \emph{equivalent} if 
    \[
      \abs{x} \leq \abs{y} \Leftrightarrow \abs{x}' \leq \abs{y}'
    \]
    holds for all $x,y\in A$.
    This defines an equivalence relation on the set of all valuations on $A$.
  \end{indefn}
  \begin{inconstruction}
    Let $(A,A^+)$ be a discrete Huber pair.
    As a set, we define 
    \[
      \spa(A,A^+)
      \defined 
      \lset 
      \abs{\cdot}
      \mc A \to \Gamma \cup \lset 0 \rset 
      \ssp 
      \abs{A^+} \leq 1
      \rset / \sim ,
    \]
    i.e. the set of equivalence classes of valuations on $A$ that are $\leq 1$ on the subring $A^+$.
    We endow $\spa(A,A^+)$ with the topology whose basis of quasi-compact opens is given by 
    \[
      U\left(\frac{g_1,\ldots,g_n}{f}\right)
      \defined 
      \lset 
      x \in \spa(A,A^+)
      \ssp 
      \abs{g_i(x)} \leq \abs{f(x)} \neq 0
      \rset.
    \]
    This topological space is called the \emph{adic spectrum} of the (discrete) Huber pair $(A,A^+)$.
    \glsadd{adic-spectrum}
    In the case that $A^+ = \wtilde{\zz}$ is the integral closure of $\zz$ in $A$, we write $\spv(A) \defined \spa(A,\wtilde{\zz})$, which is also called the \emph{valuative spectrum} of $A$.
    \glsadd{val-spec}
  \end{inconstruction}
\end{numtext}

\begin{numtext}[Relation between $\spec(A)$ and $\spa(A,A^+)$]
  Let $A$ be any ring, and $x \in \spv(A)$ a valuation on $A$.
  Then the \emph{support} of $x$, which is defined as 
  \[
    \supp(x)
    \defined 
    \lset 
    f\in A
    \ssp 
    \abs{f(x)} = 0
    \rset,
  \]
  is a prime ideal in $A$.
  So we obtain a map
  \[
    \phi\mc 
    \spv(A)
    \to 
    \spec(A)
    ,
    x 
    \mapsto 
    \supp(x).
  \]
  This map is continous, as for any distinguished open $D(f) \in A$, we have 
  \[
    \phi^{-1}(D(b))
    = 
    \lset 
    y \in \spv(A)
    \ssp 
    \abs{b(y)} \neq 0
    \rset 
    =
    \lset 
    y \in \spv(A)
    \ssp 
    \abs{0(y)}
    \leq 
    \abs{f(y)}
    \neq 
    0
    \rset 
    = 
    U(\tfrac{0}{f}),
  \]
  which is indeed open in $\spv(A)$.
  Abusing notation, we also write $\phi \mc \spa(A,A^+) \subseteq \spv(A) \to \spec(A)$ for the continous map we obtain for any choice of integral elements.
  We then have that for any discrete Huber pair, $\phi$ admits a continous section, which is given by 
  \[
    \psi 
    \mc 
    \spec(A)
    \to 
    \spa(A,A^+)
    ,~
    \idp 
    \mapsto 
    \abs{\cdot}_{\idp}
  \]
  where $s_p$ is the \emph{trivial valuation} associated to $\idp$, that is given by as the composition 
  \[
    \begin{tikzcd}[column sep = small, cramped]
    A
    \ar{r}
    &
    A/\idp
    \ar{r}
    &
    \fracfield(A/\idp)
    \ar{r}[above]{\trival}
    &
    \lset
    0,1
    \rset,
    \end{tikzcd}
  \]
  where the last map is given by $\trival(0) = 0$ and $\trival(\fracfield(A/\idp)^{\times}) = 1$.
  We can use $\phi$ to get a first idea of the ``more intricate'' nature of the topologie on $\spa(A,A^+)$:
  \begin{inexample}[c.f. \cite{sw}*{Exp. 2.3.6}]
    Consider the case $\phi \mc \spv(\zz) \to \spec(\zz)$.
    For any prime $0 \neq p \in \spec(\zz)$, the fiber $\phi^{-1}(p)$ is given by $\lset s_p,\eta_p \rset$, where $s_p$ is the trivial valuation we already encountered and $\eta_p$ is the valuation induced by the $p$-adic valuation on $\zz_p$, i.e. the composition $\zz \to \zz_p \to p^{\zz_{\leq 0}} \cup \lset 0 \rset$.
    Furthermore, we have the trivial valuation associated to zero, i.e. $\eta \mc \zz \to \lset 0,1 \rset$, mapping all non-zero non-zero integers to $1$.
  We then have that $\lset s_p \rset$ is closed, while $\overline{\lset \eta \rset} = \spv(\zz)$ and $\overline{\lset \eta_p \rset} \subseteq \lset \eta_p, s_p\rset$, which is closed, since 
  \[
    \lset \eta_p, s_p \rset 
    =
    \spv(\zz) 
    \setminus 
    \bigcup_{p \neq q}
    \lset
    x\in \spv(\zz)
    \ssp 
    \abs{q(x)}
    \leq 
    \abs{p(x)}
    \rset .
  \]
  \end{inexample}
\end{numtext}
\begin{numtext}[On $A^+$ and integrally closedness]
  Usually, the definition of a discrete Huber pair requires the subring $A^+$ to be integrally closed in $A$.
  While this is important for the more general definition in the set-up of topological rings, we don't strictly need it here:
  \begin{inlem}
    Let $(A,A^+)$ be a discrete Huber pair, and denote by $(A^+)^{\wtilde{~~}}$ the integral closure of $A^+$ in $A$.
    Then the inclusion $A^+ \hookrightarrow (A^+)^{\wtilde{~~}}$ induces a homeomorphism 
    \[
      \spa(A,A^+) 
      \isomorphism 
      \spa(A,(A^+)^{\wtilde{~~}}).
    \]
  \end{inlem}
  In light of this, we will always assume that $A^+$ is integrally closed!
  This actually suits the applications we care about quite well, since we have
  \begin{inlem}
    Let $R\to A$ be a map of finitely-generated $\zz$-algebras, and denote by $A^+ \defined \widetilde{R} \sse A$ the integral closure of $R$ in $A$.
    Then there is a canonical isomorphism of analytic rings 
    \[
      (A,R)\sqi
      \isomorphism
      (A,A^+)\sqi.
    \]
  \end{inlem}
  Recall also that we have defined $\spa(A,A^+)$ to be the set of valutations that are $\leq 1$ on $A^+$ --- a priori, there could be more elements in $A$ that also satisfy this condition for every (equivalence class of a) valuation on $A$. 
  However, this turns out not to be the case, as the following ``adic Nullstellensatz'' shows.
  For it, we use the notation $U_{1,f} \defined U(f/1)$ for $f\in A$.
  \begin{inprop}[\cite{huber-1}*{Lem. 3.3}]
    \label{nullstellen}
    Let $A$ be a discrete ring.
    Let $U \sse \spv(A)$ be a subset such that $U = \bigcap_{f\in I}U_{1,f}$ for some index set $I$.
    Then the subring $\lset f\in A \ssp \text{for all $x\in U$, $\abs{f(x)}\leq 1$}\rset$ is integrally closed in $A$.
    This assignment induces a bijection
    \[
      \begin{tikzcd}
        \lset 
        \text{subsets $U \sse \spv(A)$ such that $U = \bigcap_{f\in I}U_{f,1}$ for some $I \sse A$}
        \rset
        \ar{d}[right]{\sim}
        \\
        \lset
        \text{integrally closed subsets $A^+ \sse A$}
        \rset,
      \end{tikzcd}
    \]
    whose inverse is given by 
    $ 
    A^+ \mapsto \spa(A,A^+) \sse \spv(A)
    $.
    In particular, for any integrally closed subring $A^+ \sse A$, we have 
    \[
      A^+ = \lset f\in A \ssp \text{$\abs{f(x)} \leq 1$ for all $x\in \spa(A,A^+)$}\rset.
    \]
  \end{inprop}
  \begin{proof}
    For the first claim, we start off by noting that it is indeed a subring (as the norms interact well with multiplication and addition).
    To get that it is integrally closed, let $f \in A$ be an element that satisfies a relation of the form $f^n + a_{n-1}f^{n-1}+\ldots+a_0 = 0$ with all $a_i \in A^+$.
    Inductively, we get 
    \[
      \abs{f(x)}^n \leq \max_{i\leq n-1}(\abs{a_i(x)}\abs{f(x)}^i) \leq \max_{i\leq n-1}(\abs{f(x)}^i)
    \]
    as $\abs{a_i(x)} \leq 1$ for all $i$; hence it follows that $\abs{f} \leq 1$ holds too.
    Next, we note that the assignement $A^+ \mapsto \spa(A,A^+)$ is surjective: for any $U = \bigcap U_{f_i,1}$, we can take $A^+$ to be the integral closure of the subring generated by the $f_i$. Then $\spa(A,A^+) = U$ holds (basically, by the definition of $\spa$).
    So to finish the prove, it suffices to show 
    \[
      A^+ 
      = 
      \lset 
      f\in A
      \ssp 
      \text{
      $\abs{f(x)} \leq 1$ for all $x\in \spa(A,A^+)$
      }
      \rset.
    \]
    As noted, the inclusion ``$\sse$'' holds by definition, so we proceed by showing that for any $f\notin A^+$, there is a valuation $x \in \spa(A,A^+)$ such that $\abs{f(x)} > 1$ holds.
    By integrally closedness, we have $f\notin A^+[f^{-1}] \sse A[f^{-1}]$ (as otherwise, there would be $a\in A^+$ and $m,n$ such that $f^t(f^{m+1} - a = 0)$).
    In particular, there is a prime ideal $\idp \in A^+[f^{-1}]$ that contains $f^{-1}$.
    Let $\idq$ be a minimal prime ideal of $A^+[f^{-1}]$ that is contained in $\idp$.
    Then there is a valuation ring $V$ with a map $\spec(V) \to \spec(A^+[f^{-1}])$ taking the generic point of $V$ to $\idq$ and the special point to $\idp$ (\cite{stacks}*{\stackstag{01J8}}).
    Now the inclusion $A^+[f{-1}] \sse A[f^{-1}]$ is separated (any morphism between affines is), the valutative criterion for separatedness implies that there is a lift in the diagram 
    \[
      \begin{tikzcd}
        \spec(\fracfield(V))
        \ar{r}
        \ar{d}
        &
        \spec(A[f^{-1}])
        \ar{d}
        \\
        \spec(V)
        \ar[dashed]{ur}
        \ar{r}
        &
        \spec(A^+[f^{-1}]),
      \end{tikzcd}
    \]
    where the upper-horizontal map takes the unique point of $\fracfield(K)$ to a prime ideal in $A[f^{-1}]$ lying over $\idq$ (which exists by construction).
    Now the map $V \to A[f^{-1}]$ induces a valution on $A$ that satisfies $\abs{A^+} \sse 1$, and since $1/f\in \idp$, it takes a value $>1$ on $f$.
  \end{proof}
\end{numtext}

  Finally, we want to ``glue'' discrete adic spaces, by mimicing the construction of the structure sheaf for ordinary schemes.
  The crucial property that enables ``localizing'' of discrete adic spaces is the following:
  \begin{prop}[\cite{huber-2}*{Prop. 1.3}]
    Let $f\mc (A,A^+) \to (B,B^+)$ be a morphism of discrete adic spaces, such that there is a factorization over a rational subset of $\spa(A,A^+)$:
    \[
      \begin{tikzcd}
        \spa(B,B^+)
        \ar{r}
        \ar[dashed]{rd}
        &
        \spa(A,A^+)
        \\
        &
        U(\frac{g_1,\ldots,g_n}{f})
        \ar[hookrightarrow]{u}
      \end{tikzcd}
    \]
    Then the map $f$ factors uniquely over the pair $(A[\frac{1}{f}],A^+[\frac{g_1}{f},\ldots,\frac{g_n}{f}]^{\wtilde{~~}})$, where $A^+[\frac{g_1}{f},\ldots,\frac{g_n}{f}]^{\wtilde{~~}}$ denotes the integral closure of $A^+[\frac{g_1}{f},\ldots,\frac{g_n}{f}]$ in $A[\frac{1}{f}]$. 
    Moreover, the map 
    \[
      \spa(A[\tfrac{1}{f}],A^+[\tfrac{g_1}{f},\ldots,\tfrac{g_n}{f}]^{\wtilde{~~}})
      \to 
      \spa(A,A^+)
    \]
    is a homeomorphism onto $U(\frac{g_1,\ldots,g_n}{f})$.
  \end{prop}
\vfill\pagebreak[3]
  \begin{proof}
    \leavevmode
    \begin{itemize}
      \item 
        We first show that the map $A \to B$ factors uniquely over $A[\frac{1}{f}]$, i.e. that $f$ becomes invertible in $B$:
        To that end, we first note that the factorization of $\spa(B,B^+) \to \spa(A,A^+)$ over $U(\frac{g_1,\ldots,g_n}{f})$, we get that there is no $y\in \spa(B,B^+)$ with $\abs{f(y)} = 0$.
        For the sake of contradiction, assume now that $f$ is not invertible in $B$.
        Then we can take any point $\idp$ of $\spec(B/f)$, and the image of this point under the map 
        \[
          \spec(B/f) \sse \spec(B) \sse \spa(B,B^+)
        \]
        is a trivial valuation of the form $B \to \fracfield(B/\idp) \to \lset 0,1\rset$, any of which maps $f$ to zero (as $f\in \idp$), contradicting our initial observation.
      \item 
        Turning to the factorization over $A^+[\frac{g_1}{f},\ldots,\frac{g_n}{f}]^{\wtilde{~~}}$, we note that the factorization implies that $\abs{\frac{g_i}{f_i}(y)} \leq 1$ holds for all $y\in \spa(B,B^+)$.
        In particular, \cref{nullstellen} implies that $\frac{g_i}{f} \in B^+$ holds for all $i$, i.e. that we get a map $A^+[\frac{g_1}{f},\ldots,\frac{g_n}{f}] \to B^+$.
        Moreover, this map factors over the integral closure of the source (this is a general fact about taking integral closures), yielding our desired factorization.
      \item 
        For the last sentence, we use that for a general discrete Huber pair $(R,R^+)$, an element $f\in R$ is invertible if and only if $\abs{f(x)} \neq 0$ holds for all $x\in \spa(R,R^+)$.
        This implies that the induced map $\spa(A,A^+[\frac{g_1}{f},\ldots,\frac{g_n}{f}]^{\wtilde{~~}})$ is bijective and it can also be seen that this is indeed a homeomorphism. 
    \end{itemize}
  \end{proof}
  The above proposition implies that for any rational open subset $U \sse \spa(A,A^+)$, the pair $(A,A^+[\frac{g_i}{f}]^{\wtilde{~~}})$ is uniquely defined up to isomorphism.
  This allows us to make the following definition:

\begin{defn}
  Let $(A,A^+)$ be a discrete Huber pair, and let $X = \spa(A,A^+)$.
  Then we define presheaves $\ox,\ox^+$ on the basis of rational opens of $X$ as
  \[
    \ox(U(\tfrac{g_1,\ldots,g_n}{f}))
    \defined 
    A[\tfrac{1}{f}]
    \text{ and } 
    \ox^+(U(\tfrac{g_1,\ldots,g_n}{f}))
    \defined 
    A^+[\tfrac{g_1}{f},\ldots,\tfrac{g_n}{f}]^{\wtilde{~~}}.
  \]
\end{defn}
\begin{prop}
  The presheaves $\ox$ and $\ox^+$ are sheaves of rings on the topological space $X = \spa(A,A^+)$
  For any $x\in X$, the valuation $f\mapsto \abs{f(x)}$ extends uniquely to the local ring $\oxx$.
  For any open subset $U \sse X$, it holds that 
  \[
    \ox^+(U)
    =
    \lset 
    f\in \ox(U)
    \ssp 
    \abs{f(x)} \leq 
    \text{for all $x\in U$}
    \rset.
  \]
\end{prop}
\begin{proof}
  By the construction of the structure sheaf and the universal property of the rational subsets, we see that any valuation of the form $f\mapsto \abs{f(x)}$ extends uniquely to $\ox(U)$ if $U$ is rational and $x\in U$.
  Since we can write the (classical) local rings as colimits over all rational subsets, passing to the colimit gives the unique extension to structure sheaves.
  For the description of the rings $\ox^+(U)$, we note that this follows directly from \cref{nullstellen}.
  In particular, $\ox^+\sse \ox$ is a separated sub-presheaf, and will be a sheaf as soon as $\ox$ is.
  To establish this, we note that under the canonical map $\phi \mc \spec(A) \hookrightarrow \spa(A,A^+)$, the preimage of a rational subset $U(\frac{g_1,\ldots,g_n}{f})$ is given by the distinguished open $D(f) \sse \spec(A)$, hence $\oo_{\spa(A,A^+)} = \phi_{\ast}\oo_{\spec(A)}$.
  Since pushforwards preserve limits, this implies that $\oo_{\spa(A,A^+)}$ is indeed a sheaf.
\end{proof}
Given a discrete Huber pair $(A,A^+)$, we will often surpres the structure sheaf $\oo_{\spa(A,A^+)}$ and the collection of the valuations on the local ring from the notation, and call this data the \emph{affinoid discrete adic space} $\spa(A,A^+)$.
As for schemes, we can globalize this:
\begin{defn} 
  A \emph{discrete adic space} is a triple $(X,\ox,(\abs*{\: \cdot\: (x)})_{x\in X})$, where $X$ is a topological space, $\ox$ a sheaf of rings on $X$ and for each $x\in X$, $\abs{\: \cdot \: (x)}$ is an equivalence class of valuations on the local ring $\oxx$, subject to the condition that it is locally of the form $\spa(A,A^+)$ for an affinoid discrete Huber pair $\spa(A,A^+)$.
\end{defn}

\begin{rem}
  The ``locally'' part of the above definition is to be understood as follows:
  Consider the category of triples $(X,\ox,\abs*{\: \cdot \: (x)}_{x\in X})$ as in the first sentence of the definition (i.e. without reference to any Huber pairs).
  We then define morphisms in this category to be morphisms of ringed spaces $(\ox,X) \to (\oo_Y,Y)$, such that the diagram 
  \[
    \begin{tikzcd}
      \oo_{Y,f(x)}
      \ar{r}
      \ar{d}
      &
      \oxx
      \ar{d}
      \\
      \Gamma_{f(x)} \cup \Set{0}
      \ar{r}
      &
      \Gamma_{x} \cup \Set{0}
    \end{tikzcd}
  \]
    commutes for some representatives of the equivalence classes of representations on the local rings.
    We call an object $(X,\ox,\abs*{\: \cdot \: (x)}_{x\in X})$ in this category an \emph{discrete adic space} if there is an open cover $X = \bigcup U_i$, such that for each $i$, the triple $(U_i,\restrict{\ox}{U_i},\abs*{\: \cdot\: (x)}_{x\in U_i})$ is isomorphic (in the above category) to $\spa(A_i,A_i^+)$ for an affinoid discrete Huber pair $(A_i,A_i^+)$.
\end{rem}

\begin{prop}[\cite{huber-2}*{Prop. 2.1}]
  The functor $(A,A^+) \mapsto \spa(A,A^+)$ from the category of discrete Huber pairs to the category of discrete adic spaces is fully faithful.
\end{prop}

\begin{construction}
\leavevmode
  \begin{enumerate}
    \item 
      Let $X$ be a scheme.
      Then the presheaf that assigns to an open affine $U = \spec(A) \sse X$ the discrete adic space $\spa(A,A)$ is a sheaf for the Zariski topology on $X$, so we obtain a functor 
      \[
        \Set{\text{schemes}}\longrightarrow \Set{\text{discrete adic spaces}},
      \]
      which we denote by $X \mapsto X\adic$.
    \item 
      Fix a discrete ring $R$ and assume that $X$ is an $R$-scheme.
      Then the presheaf that assigns to an open affine $U = \spec(A) \sse X$ the discrete adic space $\spa(A,\snake{R})$ is a sheaf for the Zariski topology, and so induces a functor 
      \[
        \Set{\text{schemes over $\spec(R)$}}
        \longrightarrow
        \Set{\text{discrete adic spaces over $\spa(R,R)$}},
      \]
      which we denote by $X \mapsto X\adic,R,$.
  \end{enumerate}
\end{construction}

\begin{lem}
  \leavevmode
  \begin{enumerate}
    \item 
      Both functors $(\: - \:)\adic$ and $(\: - \:)\adic,R,$ are fully faithful.
    \item 
      Let $X$ be a scheme.
      Then points of $X\adic$ are described by the set of equivalence classes
      \[
        \Set{\text{maps $\spec(V) \to X$}\given \text{$V$ a valuation ring}}/\sim 
      \]
      where the equivalence relation is generated by declaring $\spec(V) \to X$ and $\spec(W)\to$ to be equivalent if there is a factorization 
      $ 
        \spec(W) \to \spec(V) \to X
      $
      such that the induced ring map $V\to W$ is a faithfully flat map of valuation rings.
    \item 
      Let $R$ be a discrete ring and let $X$ be an $R$-scheme.
      Then the points of $X\adic,R,$ are described by the set of equivalence classes of diagrams 
      \[
        \begin{tikzcd} 
        \spec(K) 
        \ar{r} 
        \ar{d} 
        & 
        X 
        \ar{d} 
        \\ 
        \spec(V) 
        \ar{r} 
        &
        \spec(R) 
        \end{tikzcd}
      \]
      with $V$ a valuation ring and $K = \fracfield(V)$, and the equivalence relation being the same as above.
  \end{enumerate}
\end{lem}

\begin{prop}
  Let $R$ be a separated scheme of finite type over $\spec(R)$ with $R$ a discrete ring.
  Then there is a natural open immersion $X\adic \longhookrightarrow X\adic,R,$.
  If $X$ is moreover proper over $R$, then this defines an isomorphism $X\adic \isomorphism X\adic,R,$.
\end{prop}
\begin{proof}
  We first note that that map $X\adic \to X\adic,R,$ is locally on $X\adic$ an open immersion:
  Indeed, to verify this, we can assume that $X = \spec(A)$ is affine, with $A$ an $R$-algebra of finite type, say with generators $A = \genby{f_1,\ldots,f_n}_R$.
  In that case, we have  
  \[
    \spa(A,A) = \bigcap_{i=1}^n U_{f_i,1} \sse \spa(A,R),
  \]
  which is indeed an open subset.
  \par 
  So to establish the proposition, it suffices to see that the map $X\adic \to X\adic,R,$ is injective respectively bijective.
  But this is exactly the valuative criterion for separatedness/properness (c.f. \cite{stacks}*{\stackstag{01KY}, \stackstag{0BX4}}), so we're done as we have described the points of $X\adic$ and $X\adic,R,$ in terms of the valuative criterion above.
\end{proof}
\subsection{Derived category of an adic space}

\begin{lem}
  \label{localization-compatible-with-d}
  Let $(A,A^+)$ be a discrete Huber pair, and let $U \sse X = \spa(A,A^+)$ be a rational subset.
  Then the forgetful functor 
  \[
    \derivedd((\ox(U),\ox^+(U))\ana)
    \to 
    \derivedd((A,A^+)\ana)
  \]
  is fully faithful, and admits the left-adjoint $-\dtensor_{(A,A^+)\ana} (\ox(U),\ox^+(U))\ana$.
\end{lem}

\begin{lem}
  Let $(A,A^+) \to (B,B^+)$ and $(A,A^+)\to (C,C^+)$ be morphisms of discrete Huber pairs that satisfy the following \emph{Tor-independence} condition
  \begin{align*}
    D^+
    \defined 
    B^+ \tensor_{A^+} C^+ 
    =
    B^+ \dtensor_{A^+} C^+,
  \intertext{and}
    D
    \defined 
    B\tensor_A C
    =
    B \dtensor_A C.
  \end{align*}
  Then the diagram 
  \[
    \begin{tikzcd}[column sep = 7em]
      \derivedd((B,B^+)\ana)
      \ar{r}[above]{-\tensor_{(B,B^+)\ana} (D,D^+)\ana}
      \ar[hookrightarrow]{d}
      &
      \derivedd((D,D^+)\ana)
      \ar[hookrightarrow]{d}
      \\
      \derivedd((A,A^+)\ana)
      \ar{r}[below]{-\tensor_{(A,A^+)\ana}(C,C^+)\ana}
      &
      \derivedd((C,C^+)\ana)
    \end{tikzcd}
  \]
  commutes (\coms up to natural isomorphism \come).
  Geometrically, for the cartesian diagram 
  \[
    \begin{tikzcd}      
      \spa(D,D^+)
      \ar{r}[above]{g'}
      \ar{d}[left]{f'}
      \ar[phantom]{dr}{\lrcorner}
      &
      \spa(B,B^+)
      \ar{d}[right]{f}
      \\
      \spa(C,C^+)
      \ar{r}[below]{g}
      &
      \spa(A,A^+)
  \end{tikzcd}
  \]
    there is a natural isomorphism 
    \[
      g^{\ast}f_{\ast}
      \isomorphism 
      f_{\ast}' g'^{\ast}
    \]
    of functors $\derivedd((B,B^+)\ana) \to \derivedd((C,C^+)\ana)$.
\end{lem}

\begin{proof}
  Note that we have indeed a natural transformation $g^{\ast}f_{\ast} \longrightarrow f_{\ast}'g'^{\ast}$, as the unit $\id \to g'_{\ast} g'^{\ast}$ supplies us with a natural transformation 
  \[
    \eta \mc f_{\ast} = f_{\ast} \id \longrightarrow f_{\ast} g'_{\ast} g'^{\ast} \coms = g_{\ast} f'_{\ast} g'_{\ast};\come
  \]
  and we will show that $\eta$ is an isomorphism.
\end{proof}

\begin{prop}
  Let $X = \spa (A,A^+)$ and let $M \in \derivedd((A,A^+)\ana)$.
  Assume there is an open cover $X = U_1\cup U_2 \cup \ldots \cup U_n$ where all $U_i \sse X$ are rational subsets.
  If the image of $M$ under the left-adjoints from \cref{localization-compatible-with-d} vanishes in $\derivedd((\ox(U_i),\ox^+(U_i))\ana)$ for any $i$, then $M = 0$ in $\derivedd((A,A^+)\ana)$.
\end{prop}

\begin{defn}
  Let $(A,A^+)$ be a discrete Huber pair.
  We denote by $\qder((A,A^+)\ana)$ the $\infty$-categorical enhancement of $\derivedd((A,A^+)\ana)$.
\end{defn}

\begin{theorem}
  Let $X$ be a discrete adic space.
  The association taking an open affinoid $U = \spa(A,A^+)$ to the $\infty$-category $\qder((A,A^+)\ana)$ defines a sheaf of $\infty$-categories on $X$.
\end{theorem}


\subsection{Coherent duality}
\begin{theorem}
  Let $f\mc X \to \spec(R)$ be a separated smooth map of finite type and of dimension $d$.
  Consider the line bundle $\omega_{X/R} \defined \extp^d \Omega_{X/R}^1 \in \derivedd(\oxan)$.
  There is a canonical functor 
  \[
    f_! \mc \derivedd(\oxan) \to \derivedd(R\ana)
  \]
  that agrees with $\rgam(X,-)$ in case $f$ is proper.
  The functor $f_!$ preserves compact objects.
  There is a natural trace map 
  \[
    \trace\mc f_!\omega_{X/R}[d] \to R,
  \]
  such that for all $C\in \derivedd(\oxan)$, the natural map 
  \[
    \rhom_{\ox}(C,\omega_{X/R})[d] 
    \to 
    \rhom_R(f_! C,R)
  \]
  is an isomorphism.
\end{theorem}


\begin{prop}[``Poincare-duality'']
  \label{poincare-dual}
\leavevmode
\begin{enumerate}
  \item 
    \label{poincare-dual:twist}
  Let $f\mc R \to A$ be a map of rings that is the base-change of a finitely generated map of finite Tor-dimension between noetherian rings.
  Then the natural map 
  \[
    f^!R\dtensor_{A\ana} f^{\ast} \to f^!
  \]
  of functors $D(R\ana) \to D(A\ana)$ is an equivalence.
  \item 
    \label{poincare-dual:basechange}
  Moreover, let $g\mc R\to S$ be a flat map of rings and write $f'\mc S \to B = A\tensor_R S$ for the base change of $f$ along $g$, with induced map $g'\mc A \to B$:
    \[
      \begin{tikzcd}
        R 
        \ar{r}[above]{g} 
        \ar{d}[left]{f}
        &
        S 
        \ar{d}[right]{f'}
        \\
        A 
        \ar{r}[below]{g'}
        &
        B 
      \end{tikzcd}
    \]
    Then there is a natural equivalence of functors 
    $ 
      g'^{\ast}f^! \simeq f'^{!}g^{\ast}
    $ 
    of functors $\derivedd(R\ana) \to \derivedd(B\ana)$:
    \[
      \begin{tikzcd}
        \derivedd(R\ana) 
        \ar{r}[above]{f^!}
        \ar{d}[left]{g^\ast}
        &
        \derivedd(A\ana)
        \ar{d}[right]{g'^{\ast}}
%        \ar[Rightarrow, shorten = .25em]{dl}[above left]{\sim}
        \\
        \derivedd(S\ana) 
        \ar{r}[below]{f'^{!}}
        &
        \derivedd(B\ana)
      \end{tikzcd}
    \]
  \end{enumerate}
\end{prop}
\begin{proof}
  We only show the case $A = R[t]$.
  In that case, we have $f^! R \cong R[t][1] = A[1]$ and thus also $f'^{!} S \cong B[1]$.
  Using \cref{induced-bc-behaves-well} and \eqref{fushriek-projection}, we thus have 
  \[
    g'^{\ast} f^! M = \left(g'^{\ast}(M\dtensor_{R\ana}A\ana)\right) \dtensor_{B\ana} \left(A \dtensor_{A\ana} B\ana\right)[1]
  = M \dtensor_{R\ana} B\ana [1] = f'g^{\ast}M.
  \]
\end{proof}

\begin{prop}
  \label{regclosedimmersion-dualizing}
  Let $f\mc \spec(A) \hookrightarrow \spec(R)$ be a regular closed immersion of affine schemes, that is everywhere of pure codimension $c$.
  Then there is a natural isomorphism 
  \[
    f^! R = \rhom_R(A,R) \cong \det(I/I^2)^{\vee}[-c],
  \]
  where $I = \ker(R\to A)$ and \poter$(-)^{\vee}$ is the $A$-linear dual.\come 
\end{prop}
\begin{proof}
  That $f$ is ``a regular closed immersion of pure codimension $c$'' means that we can assume locally $A = R / \genby{f_1,\ldots,f_c}$, where $f_1,\ldots,f_n$ is a regular sequence in $R$, and hence the Koszul complex $\koszul(f)$ is exact and gives a free resolution of $A$ (\cref{koszullem}), so we immediately get \[\rhom_R(A,R) = A[-c];\] note that $\rhom_R(-,A)$ shifts $\hh_0(\koszul(f)) = A$ by $-c$, since it's contravariant.
  Moreover, one can check that there is an isomorphism 
  \[
    A \isomorphism (\extp^c I/I^2)^{\vee},
  \]
  given by $1 \mapsto (f_1\wedge \ldots \wedge f_c)$, and that neither of the two isomorphisms depend on the choice of the regular sequence, so they glue.
\end{proof}
\begin{theorem}
  Let $f \mc R \to A$ be a smooth map of relative dimension $d$.
  Then there is a natural isomorphism 
  \[ 
    f^{!}R \cong \omega_{A/R}[d]
  \]
\end{theorem}
\begin{proof}
\poter  Since $\spec(A)$ is smooth over $\spec(R)$, we can find a regular closed immersion $\spec(A) \hookrightarrow \affa_R^n$ for some $n \geq d$\come.
  Let \[\Delta \mc \spec(A) \to \spec(A) \times_{\spec(R)} \spec(A)\] be the diagonal map.
  Furthermore, let \[p_1,p_2\mc \spec(A) \times_{\spec(R)} \spec(A)\to \spec(A)\] be the two projections. Note that they are in particular both retraction of $\Delta$.
  Now we calculate
  \begin{align*}
    f^! R 
    &=
    \Delta^! p_1^! f^! R 
    \\
    &=
    \Delta^! \left(p_1^{\ast} f^! R \dtensor_{(A\tensor_R A)\ana} f^! A \right),
    \intertext{by the description of $p_1^!$ from \cref{poincare-dual}, \ref{poincare-dual:twist};}
    &=
    \Delta^! \left(p_1^{\ast} f^! R \dtensor_{(A\tensor_R A)\ana} p_2^{\ast} f^! R \right) 
    \intertext{since $f^{\ast} R = A$ holds trivially, and we can then apply the base change from \cref{poincare-dual}, \ref{poincare-dual:basechange};}
    &=
    \Delta^{\ast} \left(p_1^{\ast} f^! R \dtensor_{(A\tensor_R A)\ana} p_2^{\ast} f^! R \right) 
    \dtensor_{A\ana} \Delta^! \left(A \tensor_R A\right)
    \intertext{using \cref{poincare-dual}, \ref{poincare-dual:twist} once again, but now for $\Delta$;}
    &=
    f^! R \dtensor_{A\ana} f^! R \dtensor_{A\ana} g^! (A\tensor_R A),
  \end{align*}
  using $\Delta^{\ast} p_2^{\ast} = \Delta^{\ast} p_1^{\ast} =  \id$ and that $\Delta^{\ast}$ is symmetric monoidal. 
  Now by the last part of \cref{general-affine-shriek}, \ref{general-affine-shriek:upper}, we have that $f^! R$ is invertible (since $f$ is smooth, so in particular a complete intersection), and hence the above calculation gives us (using that the inverse of an invertible object is given by its dual) 
  \[
    f^! R \cong (g^! (A \tensor_R A ))^{\vee}.
  \]
  But this we can calculate: $\Delta$ is a regular closed immersion of relative codimesion $c = n - d$, and it is a classical fact that for 
  \[
    I = \ker(\begin{tikzcd}[column sep = scriptsize, cramped] A \tensor_R A \ar{r}[above]{\text{mult}} & A \end{tikzcd}),
  \]
  i.e. $I$ being the kernel of the multiplication map, we have that $I/I^2 \cong \Omega^1_{A/R}$ --- so \cref{regclosedimmersion-dualizing} finishes the proof. 
\end{proof}

